<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato;


  class Constants {
//		const WEBMATO_TEMPLATE_DIR_NAME = '';
//		const WEBMATO_TEMPLATE_DIR_PATH = '';
    const WEBMATO_PREFIX = 'webmato__';
    const WEBMATO_PREFIX_META = 'webmato__mt__';
    const WEBMATO_PREFIX_POST = 'webmato__pt__';
    const WEBMATO_PREFIX_TAX = 'webmato__tx__';
  }
