<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  /**
   * Created by IntelliJ IDEA.
   * User: tomas
   * Date: 04.12.2016
   * Time: 21:24
   */

  namespace Webmato\Wordpress\Taxonomy;


  use Webmato\Wordpress\Helper;

  /**
   * Class TaxonomyBase
   *
   * @package Webmato\Wordpress\Taxonomy
   */
  class TaxonomyBase {

    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    protected $names_key = ['name_singular', 'name_plural', 'slug'];
    protected $auto_init;

    private $taxonomy_terms_array;

    // $domain
    /**
     * @var string $domain
     */
    protected $domain;

    // $taxonomy
    /**
     * @var string $taxonomy (required) The name of the taxonomy. Name should be in slug form (must
     *      not contain capital letters or spaces) and not more than 32 characters long (database
     *      structure restriction).
     *
     * Default: None
     */
    protected $taxonomy;

    // $taxonomy_target_names
    /**
     * @var string|array $taxonomy_target_name
     */
    protected $taxonomy_target_names;

    // $taxonomy_names;
    /**
     * @var array $names {
     *      An array of names.
     *      string $key Optional. $names['name_singular'] => Singular name of taxonomy used as
     *      singular in taxonomy labels. Leads with uppercase. string $key Optional.
     *      $names['name_plural'] => Plural name of taxonomy used as plural in taxonomy labels.
     *      Leads with uppercase. string $key Optional. $names['slug'] => Slug name. Lowercase,
     *      a-z0-9 and hyphen '-'. No spaces and underscores.
     * }
     */
    protected $taxonomy_names;

    // $taxonomy_args
    /**
     * @var array $taxonomy_args ;
     */
    protected $taxonomy_args;

    // $taxonomy_args_custom;
    /**
     * @var array $taxonomy_args_custom
     */
    protected $taxonomy_args_custom;

    // $taxonomy_args_default
    /**
     * @var array $taxonomy_args_default
     */
    protected $taxonomy_args_default;

    // $taxonomy_labels_default
    /**
     * @var array $taxonomy_labels_default
     */
    protected $taxonomy_labels_default;


    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    /**
     * Create Custom Post Type
     *
     * Constructor register new custom post type in WP.
     *
     * @param string $taxonomy              (Required)Taxonomy key word/name. Taxonomy key, must
     *                                      not exceed 32 characters.
     *
     * @param array  $taxonomy_target_names Name of the object type for the taxonomy object.
     *
     * @param array  $taxonomy_names        Optional. Default []. {
     *                                      An array of names.
     *
     * <code>
     *
     * array $taxonomy_names {
     *    string  $name_singular  (Optional)
     *                            Singular name of taxonomy used as singular in taxonomy labels.
     *                            Leads with uppercase.
     *
     *    string    $ame_plural    (Optional)
     *                             Plural name of taxonomy used as plural in taxonomy labels. Leads
     *                             with uppercase.
     *
     *    string $slug    (Optional)
     *                    Slug name. Lowercase, a-z0-9 and hyphen '-'. No spaces and underscores.
     *
     * }
     *
     * </code>
     *
     * @param array  $taxonomy_args_custom  (Optional) (Default=[])
     *                                      Arguments for register post type - same as for WP
     *                                      method reg_taxonomy()
     *
     * @param string $domain                (Optional) (Default='')
     *                                      Domain used in WP __() method
     *                                      for WP language localization.
     *
     * @param bool   $auto_init             (Optional) (Default=true)
     *                                      Post construct automatically init Taxonomy class -
     *                                      register register_taxonomy etc.
     *
     *
     * @see      https://developer.wordpress.org/reference/functions/register_taxonomy/#parameters
     *
     * @uses     __
     * @uses     register_taxonomy
     * @uses     Helper::array_merge_custom_to_default
     */
    public function __construct(
      $taxonomy,
      $taxonomy_target_names,
      $taxonomy_names = [],
      $taxonomy_args_custom = [],
      $domain = '',
      $auto_init = true
    ){
      // set properties
      $this->setTaxonomy($taxonomy);
      $this->setTaxonomyTargetNames($taxonomy_target_names);
      $this->setDomain($domain);
      $this->setTaxonomyNames($this->taxonomy, $taxonomy_names);
      $this->setTaxonomyArgsCustom($taxonomy_args_custom);
      $this->setTaxonomyArgsDefault($this->taxonomy_names);
      $this->setTaxonomyArgs($this->taxonomy_args_default, $this->taxonomy_args_custom);

      $this->auto_init = $auto_init;

      // init if $auto_init === true (default)
      if ($auto_init){
        add_action('init', [&$this, 'init']);
      }
    }


    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/
    public function init(){
      // Reg Taxonomy
      $this->reg_taxonomy($this->taxonomy, $this->taxonomy_target_names, $this->taxonomy_args);

      // Insert Taxonomy Terms
      $this->insert_custom_terms($this->taxonomy_terms_array);

      //if (is_admin()) {
      //    add_action('admin_init', array(&$this, 'init_admin'));
      //}
    }

    public function init_admin(){
    }

    /**
     * @param string       $taxonomy
     * @param string|array $taxonomy_target_names
     * @param array        $taxonomy_args Arguments for register post type - same as for WP method
     *                                    reg_taxonomy()
     *
     * @see  https://developer.wordpress.org/reference/functions/register_taxonomy/#parameters
     *
     * @uses register_taxonomy
     */
    protected function reg_taxonomy($taxonomy, $taxonomy_target_names, $taxonomy_args = null){
      register_taxonomy($taxonomy, $taxonomy_target_names, $taxonomy_args);
    }

    /**
     * Insert Custom Taxonomy Term to this Taxonomy
     *
     * Batch function to add multiple terms to this taxonomy.
     * http://codex.wordpress.org/Function_Reference/wp_insert_term
     *
     * @param array $terms_array (Required) (Default: None) Array of term arrays
     *                           <code>
     *                           $terms_array = array(
     *                           [
     *                           'term' => (int|string) (required) Default: None. The term to add
     *                           or update.
     *                           'taxonomy' => (string) (optional) Default: $this->$taxonomy
     *                           (Taxonomy provided in class constructor). The taxonomy to which to
     *                           add the term.
     *                           'getPosts_args' => (array|string) $getPosts_args (optional) Default: None. Change
     *                           the values of the inserted term.
     *                           'children_terms' => (array) $children_terms (optional) Default:
     *                           None. Array of children terms
     *                           ],
     *                           [...]
     *                           );
     *
     *      $getPosts_args = array(
     *          'alias_of' => (string) Default: None,  Accepts a term slug. Slug of the term to
     *          make this term an alias of.
     *          'description' => (string) Default: None.  The term description.
     *          'parent' => (int) Default: 0 (zero) | Id of array parent if is in $children_terms.
     *          The id of the parent term. Do not set in child $getPosts_args - it wil be replaced by parent
     *          id from the argument hierarchy.
     *          'slug' => (string) Default: None. The term slug to use
     *      );
     *
     *      $children_terms(
     *         [
     *              'term' => (int|string) (required) Default: None.
     *              'getPosts_args' => (array|string) $getPosts_args (optional) Default: None.
     *              'children_terms' => (array) $children_terms (optional) Default: None.
     *          ],
     *          [...]
     *      )
     *      </code>
     *
     * @uses insert_custom_terms
     * @uses wp_insert_term
     * @see  wp_insert_term
     */
    public function set_custom_terms($terms_array){
      $this->taxonomy_terms_array = $terms_array;
    }

    /**
     * @param     $terms_array
     * @param int $loop_level (optional) Do not set manually. Used for method repeating in the loop
     *                        purposes (parent - child loop).
     *
     * @return array|bool
     *
     * @see  set_custom_terms
     * @uses insert_custom_term_to_custom_tax
     */
    protected function insert_custom_terms(&$terms_array, $loop_level = 0){
      if ($terms_array){

        $terms_return = [];

        foreach ($terms_array as &$term){

          /*
					 * set taxonomy
					 */
          if ( !isset($term['taxonomy'])){
            $term['taxonomy'] = $this->taxonomy;
          }

          /*
					 * Insert Term to the WP DB
					 */
          $ret = $this->insert_custom_term_to_custom_tax($term);
          $ret['term'] = $term['term'];
          array_push($terms_return, $ret);
          unset($ret);

          /*
					 * If term has any children, repeat this method and insert children terms in to the WP DB.
					 */
          if (isset($term['children_terms'])){

            $parent_term = &$term;
            $children_terms = &$parent_term['children_terms'];

            //get parent term id
            $parent_ids = term_exists($parent_term['term'], $parent_term['taxonomy']);
            $parent_term_id = $parent_ids['term_id'];

            foreach ($children_terms as &$child){
              $child['getPosts_args']['parent'] = $parent_term_id;
              $child['taxonomy'] = $parent_term['taxonomy'];
            }
            unset($child);

            // Insert children terms (repeat this method - next level)
            $ret = $this->insert_custom_terms($children_terms, $loop_level + 1);
            array_push($terms_return, $ret);
            unset($ret);
          }
        }
        unset($term);

        /*
				 * Return Terms array
				 */
        if (0 === $loop_level){
          // If level is 0 array of parsed Terms and complete insert result.
          return [$terms_array, $terms_return];
        } else {
          // Return only partial insert result.
          return $terms_return;
        }
      } else {
        return false;
      }
    }


    /**
     * Insert Custom Taxonomy Term to Custom Taxonomy
     *
     * Batch function to add multiple terms to taxonomy provided in arguments.
     * http://codex.wordpress.org/Function_Reference/wp_insert_term
     *
     * @param array $terms_array (Required) (Default: None) Array of term arrays
     *                           <code>
     *                           $terms_array = array(
     *                           [
     *                           'term' => (int|string) (required) Default: None. The term to add
     *                           or update.
     *                           'taxonomy' => (string) (required) Default: None. The taxonomy to
     *                           which to add the term.
     *                           'getPosts_args' => (array|string) $getPosts_args (optional) Default: None. Change
     *                           the values of the inserted term.
     *                           ],
     *                           [...]
     *                           );
     *
     *      $getPosts_args = array(
     *          'alias_of' => (string) Default: None,  Accepts a term slug. Slug of the term to
     *          make this term an alias of.
     *          'description' => (string) Default: None.  The term description.
     *          'parent' => (int) Default: 0 (zero). The id of the parent term.
     *          'slug' => (string) Default: None. The term slug to use.
     *      )
     *      </code>
     *
     * @return array
     *
     * @uses wp_insert_term
     * @see  wp_insert_term
     */
    public static function insert_custom_terms_to_custom_tax($terms_array){
      $return_array = [];

      foreach ($terms_array as $term_array){
        $ret = self::insert_custom_term_to_custom_tax($term_array);
        array_push($return_array, $ret);
      }

      return $return_array;
    }

    /**
     * @param $term_array
     *
     * @return array|mixed|\WP_Error
     */
    protected static function insert_custom_term_to_custom_tax($term_array){
      $term = $term_array['term'];
      $taxonomy = $term_array['taxonomy'];
      $args = isset($term_array['getPosts_args']) ? $term_array['getPosts_args'] : [];

      /*
			 * Check if term already exists
			 */
      if ( !term_exists($term, $taxonomy)){
        return wp_insert_term($term, $taxonomy, $args);
      } else {
        return ['msg' => 'The term "' . $term . '" registered for "' . $taxonomy . '" already exists in WP.'];
      }

      /*
			$parent_term = term_exists('test', $taxonomy); //get parent term id
			$parent_term_id = $parent_term['term_id'];
			*/

    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/


    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/

    /**
     * @param string $domain
     */
    protected function setDomain($domain){
      $this->domain = $domain;
    }

    /**
     * @return string $domain
     */
    public function getDomain(){
      return $this->domain;
    }

    /**
     * @param string $taxonomy
     */
    protected function setTaxonomy($taxonomy){
      $this->taxonomy = $taxonomy;
      Helper::debug_print($this->taxonomy, '$this->taxonomy');
    }

    /**
     * @return string $taxonomy
     */
    public function getTaxonomy(){
      return $this->taxonomy;
    }

    /**
     * @param $taxonomy_args_default
     * @param $taxonomy_args_custom
     */
    protected function setTaxonomyArgs($taxonomy_args_default, $taxonomy_args_custom){
      $this->taxonomy_args = Helper::array_merge_custom_to_default($taxonomy_args_default, $taxonomy_args_custom);
      //DEBUG: Helper::debug_print($this->taxonomy_args, '$this->taxonomy_args');
    }

    /**
     * @return array $taxonomy_args
     */
    public function getTaxonomyArgs(){
      return $this->taxonomy_args;
    }

    /**
     * @param mixed $taxonomy_args_custom
     */
    protected function setTaxonomyArgsCustom($taxonomy_args_custom){
      $this->taxonomy_args_custom = $taxonomy_args_custom;
    }

    /**
     * @return mixed
     */
    public function getTaxonomyArgsCustom(){
      return $this->taxonomy_args_custom;
    }

    /**
     * @param array $taxonomy_names
     */
    protected function setTaxonomyArgsDefault($taxonomy_names){
      $this->setTaxonomyLabelsDefault($taxonomy_names);

      $this->taxonomy_args_default = [
        /*
				 * (string) (optional) A plural descriptive name for the taxonomy marked for translation.
				 *
				 * Default: overridden by $labels->name
				 */
        'label' => $taxonomy_names['name_plural'],

        'labels'            => $this->getTaxonomyLabelsDefault(),

        /*
				 * (boolean) (optional) Should this taxonomy be exposed in the admin UI.
				 *
				 * Default: true
				 */
        'public'            => true,

        /*
				 * (boolean) (optional) Whether to generate a default UI for managing this taxonomy.
				 *
				 * Default: if not set, defaults to value of public argument. As of 3.5, setting this
				 * to false for attachment taxonomies will hide the UI.
				 */
        'show_ui'           => true,

        /*
				 * (boolean) (optional) true makes this taxonomy available for selection in navigation menus.
				 *
				 * Default: if not set, defaults to value of public argument
				 */
        //'show_in_nav_menus' => true,

        /*
				 * (boolean) (optional) Whether to allow the Tag Cloud widget to use this taxonomy.
				 *
				 * Default: if not set, defaults to value of show_ui argument
				 */
        //'show_tagcloud' => true,

        /*
				 * (callback) (optional) Provide a callback function name for the meta box display. (Available since 3.8)
				 *
				 * Default: null
				 *
				 * Note: Defaults to the categories meta box for hierarchical taxonomies and the tags meta box
				 * for non-hierarchical taxonomies. No meta box is shown if set to false.
				 */
        //????
        //'meta_box_cb' => null,

        /*
				 * (boolean) (optional) Whether to allow automatic creation of taxonomy columns on associated
				 * post-types. (Available since 3.5)
				 *
				 * Default: false
				 */
        'show_admin_column' => true,

        /*
				 * (boolean) (optional) Is this taxonomy hierarchical (have descendants) like categories or not
				 * hierarchical like tags.
				 *
				 * Default: false
				 *
				 * Note: Hierarchical taxonomies will have a list with checkboxes to select an existing
				 * category in the taxonomy admin box on the post edit page (like default post categories).
				 * Non-hierarchical taxonomies will just have an empty text field to type-in taxonomy terms
				 * to associate with the post (like default post tags).
				 */
        'hierarchical'      => false,

        /*
				 * (string) (optional) A function name that will be called when the count of an associated
				 * $object_type, such as post, is updated. Works much like a hook.
				 *
				 * Default: None - but see Note, below.
				 *
				 * Note: While the default is '', when actually performing the count update in wp_update_term_count_now(),
				 * if the taxonomy is only attached to post types (as opposed to other WordPress objects, like user),
				 * the built-in _update_post_term_count() function will be used to count only published posts associated
				 * with that term, otherwise _update_generic_term_count() will be used instead, that does no such checking.
				 *
				 * This is significant in the case of attachments. Because an attachment is a type of post, the default
				 * _update_post_term_count() will be used. However, this may be undesirable, because this will only count
				 * attachments that are actually attached to another post (like when you insert an image into a post).
				 * This means that attachments that you simply upload to WordPress using the Media Library, but do not
				 * actually attach to another post will not be counted. If your intention behind associating a taxonomy
				 * with attachments was to leverage the Media Library as a sort of Document Management solution, you are
				 * probably more interested in the counts of unattached Media items, than in those attached to posts.
				 * In this case, you should force the use of _update_generic_term_count() by setting '_update_generic_term_count'
				 * as the value for update_count_callback.
				 */
        //'update_count_callback' => '',

        /*
				 * (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var
				 * instead of default which is $taxonomy, the taxonomy's "name".
				 *
				 * Default: $taxonomy
				 *
				 * Note: The query_var is used for direct queries through WP_Query like new
				 * WP_Query(array('people'=>$person_name)) and URL queries like /?people=$person_name. Setting query_var
				 * to false will disable these methods, but you can still fetch posts with an explicit WP_Query taxonomy
				 * query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
				 */
        //'query_var' => $taxonomy,

        /*
				 * (boolean/array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks".
				 * Pass an $getPosts_args array to override default URL settings for permalinks as outlined below:
				 *
				 * Default: true
				 *
				 * 'slug' - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
				 * 'with_front' - allowing permalinks to be prepended with front base - defaults to true
				 * 'hierarchical' - true or false allow hierarchical urls (implemented in Version 3.1) - defaults to false
				 * 'ep_mask' - Assign an endpoint mask for this taxonomy - defaults to EP_NONE. For more info see this Make WordPress
				 * Plugins summary of endpoints.
				 *
				 * Note: You may need to flush the rewrite rules after changing this. You can do it manually by going to the Permalink
				 * Settings page and re-saving the rules -- you don't need to change them -- or by calling $wp_rewrite->flush_rules().
				 * You should only flush the rules once after the taxonomy has been created, not every time the plugin/theme loads.
				 */
        'rewrite'           => [
          'slug' => $taxonomy_names['slug'],
          //'with_front' => true,
          //'hierarchical' => false,
          //'ep_mask' => EP_NONE
        ],

        /*
				 * (array) (optional) An array of the capabilities for this taxonomy.
				 *
				 * Default: None
				 *
				 * 'manage_terms' - 'manage_categories'
				 * 'edit_terms' - 'manage_categories'
				 * 'delete_terms' - 'manage_categories'
				 * 'assign_terms' - 'edit_posts'
				 */
        //'capabilities' => [],

        /*
				 * (boolean) (optional) Whether this taxonomy should remember the order in which terms are added to objects.
				 *
				 * Default: None
				 */
        //'sort' => true,

        /*
				 * (boolean) (not for general use) Whether this taxonomy is a native or "built-in" taxonomy.
				 * Note: this Codex entry is for documentation - core developers recommend you don't use this
				 * when registering your own taxonomy
				 *
				 * Default: false
				 */
        //'_builtin' => false
      ];

      Helper::debug_print($this->taxonomy_args_default, '$this->post_args_default');
    }

    /**
     * @return array
     */
    public function getTaxonomyArgsDefault(){
      return $this->taxonomy_args_default;
    }

    /**
     * @param $taxonomy_names
     *
     * @internal param mixed $taxonomy_labels_default
     */
    protected function setTaxonomyLabelsDefault($taxonomy_names){
      $domain = $this->domain;

      $taxonomy_labels_default = [
        'name'              => __($taxonomy_names['name_plural'], $domain),
        'singular_name'     => __($taxonomy_names['name_singular'], $domain),
        'menu_name'         => __($taxonomy_names['name_plural'], $domain),
        'all_items'         => __('All ' . $taxonomy_names['name_plural'], $domain),
        'edit_item'         => __('Edit ' . $taxonomy_names['name_singular'], $domain),
        'view_item'         => __('View ' . $taxonomy_names['name_singular'], $domain),
        'update_item'       => __('Update ' . $taxonomy_names['name_singular'], $domain),
        'add_new_item'      => __('Add New ' . $taxonomy_names['name_singular'], $domain),
        'new_item_name'     => __('New ' . $taxonomy_names['name_singular'] . ' Name', $domain),
        'parent_item'       => __('Parent ' . $taxonomy_names['name_singular'], $domain),
        'parent_item_colon' => __('Parent ' . $taxonomy_names['name_singular'] . ':', $domain),
        'search_items'      => __('Search ' . $taxonomy_names['name_plural'], $domain),
      ];

      $this->taxonomy_labels_default = $taxonomy_labels_default;
      Helper::debug_print($this->taxonomy_labels_default, '$this->taxonomy_labels_default');
    }

    /**
     * @return mixed
     */
    public function getTaxonomyLabelsDefault(){
      return $this->taxonomy_labels_default;
    }

    /**
     * Parse or generate plural, singular and slug names
     *
     * @since   1.0.0
     * @version 1.0.0
     *
     * @param string $base_name
     * @param array  $names
     *
     * @throws \Exception Only a string in $base_name and an array of strings in $names is accepted.
     *
     * @see     Helper::str_wp_normal
     * @see     __construct
     * @see     taxonomy_names
     */
    protected function setTaxonomyNames($base_name, $names = []){
      $domain = $this->domain;

      if (is_string($base_name) && is_array($names)){
        //If is name doesn't exists
        foreach ($this->names_key as $key => $val){
          if ( !array_key_exists($val, $names) || '' === $names[$val]){
            // if singular name exists but plural not
            if ('name_plural' === $val && ($names['name_singular'] && '' !== $names['name_singular'])){
              $names[$val] = $names['name_singular'];
            } else {
              $names[$val] = $base_name;
            }
          }
        }

        $names['name_singular'] = ucwords(__($names['name_singular'], $domain));
        $names['name_plural'] = ucwords(__($names['name_plural'], $domain));
        $names['slug'] = Helper::str_wp_normal($names['slug']);

      } else {
        throw new \Exception('Only a string in $base_name and an array of strings in $names is accepted.');
      }

      $this->taxonomy_names = $names;
      Helper::debug_print($this->taxonomy_names, '$this->taxonomy_names');
    }

    /**
     * @return array $taxonomy_names
     */
    public function getTaxonomyNames(){
      return $this->taxonomy_names;
    }

    /**
     * @param array|string $taxonomy_target_names
     */
    public function setTaxonomyTargetNames($taxonomy_target_names){
      $this->taxonomy_target_names = $taxonomy_target_names;
    }

    /**
     * @return array|string $taxonomy_target_name
     */
    public function getTaxonomyTargetNames(){
      return $this->taxonomy_target_names;
    }

  }
