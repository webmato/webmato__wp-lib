<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress;

  /**
   * Class Helper
   *
   * @package Webmato\Wordpress
   */
  class Helper {
    /**
     * Normalize string to WP recommended style - lowercase, no space, no underscores.
     * String to lowercase, replace white space ' ' by '-', replace underscores '_' by '-'.
     *
     *
     * @param  string $str String to normalize.
     *
     * @return string Normalized string.
     */
    public static function str_wp_normal($str){
      // to lower case
      $str = strtolower($str);

      // replace spaces with hyphen
      $str = str_replace(" ", "-", $str);

      // replace underscore with hyphen
      $str = str_replace("_", "-", $str);

      return $str;
    }


    /**
     * Debug/print $var
     *
     * Stylize print_r() with <pre> for fast debug. Set define define('TOMATO_DEBUG', true or
     * false); to on/off printing the debug info.
     *
     * @param mixed   $var              Variable to print.
     * @param string  $info_str         Info string to print in header
     * @param boolean $force_dbg_on_off Optional. Default 'null'. Force to show debug info over the
     *                                  TOMATO_DEBUG property /define('TOMATO_DEBUG', true or
     *                                  false)/.
     */
    public static function debug_print($var, $info_str = '', $force_dbg_on_off = null){
      if (defined('WEBMATO_DEBUG') && WEBMATO_DEBUG || true === $force_dbg_on_off){
        if (false !== $force_dbg_on_off){
          echo("<pre style='background-color: orange; padding-left: 200px'>");
          echo("<h2 style=''>$info_str</h4>");
          print_r($var);
          echo("</pre>");
        }
      }
    }

    /**
     * @param array $default
     * @param array $custom
     *
     * @return array
     */
    public static function array_merge_custom_to_default($default, $custom){
      $new_array = $default;

      foreach ($custom as $key => $value){

        // if isset in the defaults array
        if (isset($default[$key])){

          // if this option is a nested array
          if (is_array($default[$key])){

            // repeat the process
            $new_array[$key] = Helper::array_merge_custom_to_default($default[$key], $custom[$key]);

            // else if the value is not an array
          } else {

            // override with user submitted option
            $new_array[$key] = $custom[$key];

          }

          // else if the default has not been set
        } else {
          // add it to the option
          $new_array[$key] = $value;
        }
      }

      return $new_array;
    }

    /**
     * @param        $title
     * @param        $position
     * @param string $customCss
     * @param bool   $autoEcho
     *
     * @return null|string
     */
    static function visualBlockSeparator($title, $position, $customCss = '', $autoEcho = true){
      $element = null;
      $style = "";

      $style_visualBlockSeparator = "" .
                                    //.debug.visual-block-separator
                                    "background-color: rgba(255, 0, 0, 0.1);" .
                                    "color: red;" .
                                    "display: block;" .
                                    "text-align: center;" .
                                    "width: 100%;";

      $style_visualBlockSeparator_start = "border-top: dashed 1px red;";
      $style_visualBlockSeparator_end = "border-bottom: dashed 1px red;";

      $style_visualBlockSeparator_templatePartial = "" .
                                                    "background-color: rgba(0, 0, 255, 0.1);" .
                                                    "color: blue;";


      if (defined('WEBMATO_DEBUG__HTML_STRUCTURE') && WEBMATO_DEBUG__HTML_STRUCTURE){
        switch ($position){
          case 'start': {
            $title = " | --- $title >>>";
            $style = "$style_visualBlockSeparator $style_visualBlockSeparator_start";
            break;
          }
          case 'end': {
            $title = " <<< $title ---|";
            $style = "$style_visualBlockSeparator $style_visualBlockSeparator_end";
            break;
          }
          default: {
            //
          }
        }

        $element = "<div class=\"debug visual-block-separator visual-block-separator--$position $customCss\" style=\"$style\">" .
                   "$title" .
                   "</div >";

        if ($autoEcho){
          echo($element);
        }
      }

      return $element;
    }

    /**
     * @param        $title
     * @param string $customCss
     * @param bool   $autoEcho
     *
     * @return null|string
     */
    public
    static function visualBlockSeparator_start($title, $customCss = '', $autoEcho = true
    ){
      return Helper::visualBlockSeparator($title, 'start', $customCss, $autoEcho);
    }

    /**
     * @param        $title
     * @param string $customCss (Optional) (Default: '')
     * @param bool   $autoEcho  (Optional) (Default: true)
     *
     * @return null|string
     */
    public
    static function visualBlockSeparator_end($title, $customCss = '', $autoEcho = true
    ){
      return Helper::visualBlockSeparator($title, 'end', $customCss, $autoEcho);
    }
  }
