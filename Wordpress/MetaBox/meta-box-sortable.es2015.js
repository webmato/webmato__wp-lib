/*
 * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
 */

jQuery(document).ready(($) => {
  /*************************************************************************************************
   *                                      Common Variables
   ************************************************************************************************/
  // DOM selectors
  /** @const {String} */
  const sortable_domSelector = '#sortable';
  /** @const {String} */
  const sortableContainerSelection_domSelector = '#sortable_container_selection';
  /** @const {String} */
  const sortableContainerResult_domSelector = '#sortable_selection_result';
  /** @const {String} */
  const sortableContainerOptions_domSelector = '#sortable_container_options';
  /** @const {String} */
  const sortableInput_domSelector = '.sortable_input';

  // jQ Elements
  /** @type {JQuery} */
  let $_sortable_container_selection;
  /** @type {JQuery} */
  let $_sortable_container_options;


  /*************************************************************************************************
   *                                          Methods
   ************************************************************************************************/

  /**
   *
   */
  function resizeContainers() {
    /** @const {Number} */
    const container_selection_height = $_sortable_container_selection.height();
    /** @const {Number} */
    const container_selection_count = $_sortable_container_selection.children().length;
    /** @const {Number} */
    const container_options_height = $_sortable_container_options.height();
    /** @const {Number} */
    const container_options_count = $_sortable_container_options.children().length;

    if (container_options_count > container_selection_count) {
      $_sortable_container_options.css('height', 'auto');
      $_sortable_container_selection.height(container_options_height);
    } else {
      $_sortable_container_selection.css('height', 'auto');
      $_sortable_container_options.height(container_selection_height);
    }
  }

  /**
   * @param {JQueryUI.SortableEvent} event
   * @param {Object} ui
   *
   * @link https://api.jqueryui.com/sortable/#event-update
   */
  function update_selection(event, ui) { // eslint-disable-line no-unused-vars
    // Get all selected items
    /** @type {JQuery} */
    const $_selection = $(sortableContainerSelection_domSelector).children();
    /** @type {JQuery} */
    const $_inputs = $_selection.find(sortableInput_domSelector);

    // Get values of selected items
    /** @const {Array} */
    const selection_input_values = [];
    $_inputs.each(/** Number, HTMLElement */(index, sortable_elm) => {
      selection_input_values.push($(sortable_elm).val());
    });
    /** @const {String} */
    const values_string = JSON.stringify(selection_input_values);

    // Set all selected items values to selection input
    /** @const {JQuery} */
    const $_selection_result_input = $(sortableContainerResult_domSelector);
    $_selection_result_input.val(values_string);

    resizeContainers();
  }

  /**
   *
   */
  function init() {
    $_sortable_container_selection = $(sortableContainerSelection_domSelector);
    $_sortable_container_options = $(sortableContainerOptions_domSelector);
    resizeContainers();

    /***********************************************************************************************
     * Setup Sortable jQuery UI widget
     *
     * @link https://api.jqueryui.com/sortable/
     */
    $_sortable_container_selection.sortable({
      connectWith: sortableContainerOptions_domSelector,
      update: (event, ui) => update_selection(event, ui),
      cursor: 'move',
    });

    $_sortable_container_options.sortable({
      connectWith: sortableContainerSelection_domSelector,
      cursor: 'move',
    });

    // Init Sortable jQuery UI widget
    $(sortable_domSelector).sortable();
  }


  /*************************************************************************************************
   *                                    Event Handlers
   ************************************************************************************************/
  function onWindowResize() {
    resizeContainers();
  }


  /*************************************************************************************************
   *                                     Controllers
   ************************************************************************************************/
  $(window).resize(onWindowResize);


  /*************************************************************************************************
   *                                        Init
   ************************************************************************************************/
  init();
});
