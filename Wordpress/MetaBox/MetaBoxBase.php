<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\MetaBox;

  /**
   * Class MetaBoxBase
   *
   * @package Webmato\Wordpress\MetaBox
   */
  class MetaBoxBase {

    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/

    /** @var string $meta_IdName_base */
    protected $meta_IdName_base;

    /**
     * @var string $html_id
     * Meta box ID (used in the 'html_id' attribute for the meta box).
     * @see @title https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $html_id
     */
    protected $id;

    /**
     * @var string $title
     * Title of the meta box.
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $title
     */
    protected $title;

    /**
     * @var array $callback
     *      Function that prints out the HTML for the
     *      edit screen section. The function name as a string, or, within a class, an array to
     *      call one of the class's methods. The callback can accept up to two arguments: the first
     *      argument is the $post object for the post or page that is currently being edited. The
     *      second argument is the full $metabox item (an array), see Callback getPosts_args.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_meta_box#Parameters -> $callback
     */
    protected $callback;


    /**
     * @var array $screen
     * The screen or screens on which to show the box (such as a post type, 'link', or 'comment').
     * Accepts a single screen ID, WP_Screen object, or array of screen IDs. Default is the current
     * screen.
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $screen
     */
    protected $screen;

    /**
     * @var string $context
     * The context within the screen where the boxes should display. Available contexts vary from
     * screen to screen. Post edit screen contexts include 'normal', 'side', and 'advanced'.
     * Comments screen contexts include 'normal' and 'side'. Menus meta boxes (accordion sections)
     * all use the 'side' context.
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $context
     */
    protected $context;

    /**
     * @var string $priority
     * The priority within the context where the boxes should show ('high', 'low').
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $priority
     */
    protected $priority;

    /** @var array $callback_args */
    protected $callback_args;

    /** @var string $domain */
    protected $domain;

    /** @var bool $auto_init */
    protected $auto_init;

    /** @var string $WEBMATO_PREFIX_META */
    private $WEBMATO_PREFIX_META;

    /***********************************************************************************************
     *                                        CONSTRUCTOR
     **********************************************************************************************/

    /**
     * MetaBoxBase constructor.
     *
     * @param string       $meta_IdName_base      (required) (Default: -)
     *                                            Base name for auto generating meta key, css
     *                                            html_id etc.
     *
     * @param string|array $post_type             (required) (Default: -)
     *                                            The type of Write screen on which to show the
     *                                            edit
     *                                            screen section ('post', 'page', 'dashboard',
     *                                            'link', 'attachment' or 'custom_post_type' where
     *                                            custom_post_type is the custom post type slug)
     *
     * @param string|null  $html_id               (required) (Default: -)
     *                                            Meta box ID (used in the 'html_id' attribute for
     *                                            the meta box).
     *
     * @param string       $ui_title              (required) (Default: -)
     *                                            Title of the meta box.
     *
     * @param array        $admin_ui_args         (Optional) (Default: ['context' => 'side',
     *                                            'priority' => 'default'])
     *
     * <code>
     * {
     *
     * string   $priority  (Optional) (Default: 'default')
     *                     The priority within the context where the boxes should show ('high',
     *                     'core', 'default' or 'low')
     *                     https://developer.wordpress.org/reference/functions/add_meta_box/#parameters
     *                     ->
     *                     $priority
     *
     * string   $context   (Optional) (Default: 'side')
     *                     The part of the page where the edit screen section should be shown
     *                     ('normal', 'advanced', or 'side').
     *                     https://developer.wordpress.org/reference/functions/add_meta_box/#parameters
     *                     ->
     *                     $context
     *
     * }
     * </code>
     *
     * @param array        $metabox_properties    (Optional) (Default: [])
     *                                            Arguments passed into callback function. In
     *                                            callback function get $metabox_properties this
     *                                            way:
     *                                            <code>
     *                                            function callback($post, $meta_box_args){
     *                                            $metabox_properties = $meta_box_args['getPosts_args'];
     *                                            </code>
     *                                            https://developer.wordpress.org/reference/functions/add_meta_box/#parameters
     *                                            -> $callback_args
     *
     * <code>
     * {
     *
     * array[$meta_box]   $meta_boxes (Required)
     * [
     *
     *    array $meta_box
     *    {
     *
     *        string   $type    (Required)
     *                          Meta box types: "_group", "text", "gallery", "select", "wp_editor",
     *                          "tax_select", "tax_checkbox", "tax_radio" ...
     *
     *        string   $key     (Optional: auto generated)
     *                          Meta key name. No white space allowed here and lowercase.
     *
     *        string   $html_id      (Optional: auto generated)
     *                          CSS html_id
     *
     *        string   $label   (Optional)
     *
     *        string   $desc    (Optional)
     *                          Description text
     *
     *    }
     * ]
     *    any     $custom (Optional) (Default: -)
     *
     * }
     * </code>
     *
     * @param array        $callback_function     (Optional) (Default: [])
     *                                            Function that prints out the HTML for the edit
     *                                            screen section. The function name as a string,
     *                                            or, within a class, an array to call one of the
     *                                            class's methods. The callback can accept up to
     *                                            two arguments: the first argument is the $post
     *                                            object for the post or page that is currently
     *                                            being edited. The second argument is the full
     *                                            $metabox item (an array), see Callback getPosts_args.
     *                                            <code>
     *                                            $callback = array(
     *                                            &$this,
     *                                            'meta_box_callback'
     *                                            );
     *                                            </code>
     *                                            https://developer.wordpress.org/reference/functions/add_meta_box/#parameters
     *                                            -> $callback
     *
     * <code>
     * {
     *
     * callable   $key=0    (required) (Default: -)
     *                      Parent object of callback function "&$this".
     *
     * string   $key=1      (required) (Default: -)
     *                      Name of callback function.
     *
     * }
     * </code>
     *
     * @param string       $domain                (optional) (Default: '') Domain used in WP __()
     *                                            method for WP language localization.
     * @param boolean      $auto_init             (optional) (Default: true)
     *
     *
     * @link                                    http://codex.wordpress.org/Function_Reference/add_meta_box
     * @link                                    http://codex.wordpress.org/Function_Reference/add_meta_box#Parameters
     * @link                                    http://codex.wordpress.org/Function_Reference/add_meta_box#Callback_args
     */
    public function __construct(
      $meta_IdName_base,
      $post_type,
      $html_id = null,
      $ui_title,
      $admin_ui_args = ['context' => 'side', 'priority' => 'default'],
      $metabox_properties = [],
      $callback_function = [],
      $domain = '',
      $auto_init = true
    ){
      if (defined('WEBMATO_PREFIX_META')){
        $this->WEBMATO_PREFIX_META = WEBMATO_PREFIX_META;
      } else {
        $this->WEBMATO_PREFIX_META = '';
      }
      //
      $this->meta_IdName_base = $meta_IdName_base;
      $this->setScreen($post_type);
      $this->setId($html_id);
      $this->title = $ui_title;
      $this->setAdminUiArgs($admin_ui_args['context'], $admin_ui_args['priority']);
      $this->setCallbackArgs($metabox_properties);
      $this->setCallback($callback_function);
      $this->domain = $domain;
      $this->auto_init = $auto_init;


      if ($this->auto_init){
        $this->init();
      }
    }


    /***********************************************************************************************
     *                                        METHODS
     **********************************************************************************************/

    public function init(){
      if (is_admin()){
        //add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
        //add_action('admin_head', array($this, 'admin_head'));
        add_action('add_meta_boxes', [$this, 'add_box']);
        add_action('save_post', [$this, 'save_meta_boxes']);

      }
    }

    //______________________________________________________________________________________________
    // ADMIN
    public function add_box(){
      foreach ($this->screen as $screen){
        add_meta_box(
          $this->id
          , __($this->title, $this->domain)
          , $this->callback
          , $screen
          , $this->context
          , $this->priority
          , $this->callback_args
        );
      }
    }

    /**
     * @param $post
     * @param $args_metabox
     */
    public function meta_box_callback($post, $args_metabox){
      echo($this->meta_boxes_build($post, $args_metabox['getPosts_args']['meta_boxes']));
    }

    /**
     * @param      $post
     * @param      $meta_boxes_array
     * @param null $css_class
     * @param bool $group_meta_key
     *
     * @internal param $args_metabox
     * @return null|string
     */
    protected function meta_boxes_build($post, $meta_boxes_array, $css_class = null, $group_meta_key = false){
      $meta_boxes_html = null;

      // for each meta box get specific meta box arguments/options
      $num = 0;
      foreach ($meta_boxes_array as $meta_box){
        if ( !isset($meta_box['id'])){
          $meta_box['id'] = '';
        }
        if ($num > 0){
          $meta_boxes_html .= '<hr />';
        }

        $css_class = isset($css_class) ? $css_class : 'meta-box-' . esc_attr($meta_box['type']);
        $meta_box_label_html = isset($meta_box['label']) ? '<h4>' . $meta_box['label'] . '</h4>' : '';
        $meta_box_desc_html = isset($meta_box['desc']) ? '<p>' . $meta_box['desc'] . '</p>' : '';

        $meta_boxes_html .=
          '
          <div id="' . esc_attr($meta_box['id']) . '" class="' . $css_class . '" style="background-color: #fff; padding: 0px;">
            ' . $meta_box_label_html . '
            ' . $meta_box_desc_html . '
            <div>
              ' . $this->add_custom_meta_box($post, $meta_box, $group_meta_key) . '
            </div>
          </div>
          ';
        $num ++;
      }

      return $meta_boxes_html;
    }

    /**
     * @param $post
     * @param $meta_box
     * @param $group_meta_key
     *
     * @throws \Exception
     * @return string
     */
    public function add_custom_meta_box($post, $meta_box, $group_meta_key = null){
      $post_id = get_the_ID();

      /*
			 * Check if Meta Box type is provided
			 */
      if (isset($meta_box['type'])){
        $type = $meta_box['type'];
      } else {
        throw new \Exception('Meta type is not set. The meta type must be set and provided in call back arguments $callback_arg["meta"][["type"]] = meta_type.');
      }

      /*
			 * Check if Meta key name is provide
			 */
      if (isset($meta_box['key'])){
        $meta_key = $this->normalize_str($meta_box['key']);
      } else if ('_group' === $type){
        $meta_key = null;
      } else {
        throw new \Exception('The meta name must be provided in $callback_arg["key"] = meta_keyname.');
      }

      $settings = isset($meta_box['settings']) ? $meta_box['settings'] : null;
      $css_id = isset($meta_box['id']) ? $meta_box['id'] : 'meta_' . $meta_key;

      /*
			 * Get meta value from WP database
			 */
      // if group has own meta key, the of the meta box are serialized and stored in DB under $group_meta_key
      $meta_value = null;
      if ($group_meta_key){
        // get group meta value and unserialize
        $meta_value_serial = get_post_meta($post_id, $group_meta_key, true);
        $meta_value_array = unserialize($meta_value_serial);
        // get value for current meta box from the group
        $meta_value = isset($meta_value_array[$meta_key]) ? $meta_value_array[$meta_key] : null;
      } else if ($meta_box['type'] != '_group'){
        // get directly form DB
        $meta_value = get_post_meta($post_id, $meta_key, true);
      }
      if ($meta_value == ''){
        $meta_value = null;
      }

      /*
			 * Generate Meta Box html
			 */
      switch ($type){
        // Group: group of meta boxes
        case '_group' :
          return $this->generate_metabox_type__group($meta_box, $meta_key, $css_id, $meta_value, $settings, $post);

        // Text: basic text input
        case 'text' :
          return $this->generate_metabox_type_text($meta_box, $meta_key, $css_id, $meta_value, $settings);

        // Gallery:
        case 'gallery' :
          return $this->generate_metabox_type_gallery($meta_box, $meta_key, $css_id, $settings);

        // Select: input select (drop box)
        case 'select' :
          return $this->generate_metabox_type_select($meta_box, $meta_key, $css_id, $meta_value, $settings);

        case 'checkbox' :
          return $this->generate_metabox_type_checkbox($meta_box, $meta_key, $css_id, $meta_value, $settings);

        // Sortable:
        case 'sortable' :
          return $this->generate_metabox_type_sortable($meta_box, $meta_key, $css_id, $meta_value, $settings);

        // Sortable selection:
        case 'sortable_select' :
          return $this->generate_metabox_type_sortableselect($meta_box, $meta_key, $css_id, $meta_value, $settings);

        // WP Editor: wordpress editor
        case 'wp_editor' :
          return $this->generate_metabox_type_wp_editor($meta_box, $meta_key, $css_id, $meta_value, $settings);

        // TODO: implement "tax_select" meta box
        // Taxonomy Select:
        case 'tax_select' :
          $tax_key = $meta_key;
          $tax_terms = get_terms($tax_key, 'get=all');

          return $this->generate_metabox_type_tax_select($meta_box, $tax_key, $css_id, $tax_terms, $settings);

        // TODO: implement "tax_checkbox" meta box
        // Taxonomy CheckBox
        case 'tax_checkbox' :
          $tax_key = $meta_key;
          $tax_terms = get_terms($tax_key, ['orderby' => 'id', 'get' => 'all']);

          return $this->generate_metabox_type_tax_checkbox($meta_box, $tax_key, $css_id, $tax_terms, $settings);

        // TODO: implement "tax_radio" meta box
        // Taxonomy Radio
        case 'tax_radio' :
          $tax_key = $meta_key;

          return null;

        // meta type not supported
        default:
          return '<p style="color: red">Error! Unknown Meta Box Type: "' . $type . '". This Meta Box type is not supported.</p>';
      }
      //end switch


    }

    /**
     * @param $post_id
     */
    public function save_meta_boxes($post_id){
      //TODO add verify nonce

      // check autosave
      if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
        return;
      }

      // Check the user's permissions.
      if (isset($_POST['screen']) && 'page' == $_POST['screen']){

        if ( !current_user_can('edit_page', $post_id)){
          return;
        }
      } else {

        if ( !current_user_can('edit_post', $post_id)){
          return;
        }
      }

      $this->save_meta($this->callback_args['meta_boxes'], $post_id);
    }

    /**
     * @param $meta_boxes_array
     * @param $post_id
     */
    protected function save_meta($meta_boxes_array, $post_id){
      foreach ($meta_boxes_array as $meta_box){

        $meta_key = isset($meta_box['key']) ? $this->normalize_str($meta_box['key']) : null;
        $meta_type = $meta_box['type'];
        $new_val = null;
        $old_val = get_post_meta($post_id, $meta_key, true);

        /*
				 * Collect new value of the meta from the web form meta box ($POST[$meta_key])
				 */
        switch ($meta_type){
          case '_group' : {
            $meta_boxes = $meta_box['settings']['meta_boxes'];

            // If $meta_key of meta box group is provided - save serialized all meta inputs under one meta key
            if ($meta_key){
              // -> serialize data and save in meta under this $meta_key
              $new_val_array = [];
              foreach ($meta_boxes as $m_box){
                $m_box_key = $this->normalize_str($m_box['key']);
                $new_val_array[$m_box_key] = $this->collect_posted_value($m_box_key);
              }
              $new_val = serialize($new_val_array);
              $this->delete_or_update_metavalue($post_id, $meta_key, $new_val, $old_val);
            } else {
              // -> recursively rerun save_meta to save each meta box under separate meta value
              $this->save_meta($meta_boxes, $post_id);
            }
            break;
          }
          case 'tax_checkbox' : {
            $tax_key = $meta_key;
            $tax_terms = get_terms($tax_key, [
              'orderby' => 'id',
              'get'     => 'all',
            ]); // get all taxonomy terms
            $new_val = [];

            foreach ($tax_terms as $key => $term_val){
              $term_id = $term_val->term_id;

              // resolve $_POST key of the term
              $server_post_key = $meta_key . '-id_' . $term_id;

              // get new value from $_POST
              $new_val_part = $this->collect_posted_value($server_post_key);
              if ($new_val_part){
                array_push($new_val, (int) $new_val_part);
              }
            }
            unset($key);
            unset($term_val);
            unset($term_id);
            unset($new_val_part);
            unset($server_post_key);
            unset($tax_terms);

            break;
          }
          default:
            $new_val = $this->collect_posted_value($meta_key);
            break;
        }

        /*
				 * If regular Meta box type or Taxonomy viewed in meta box.
				 * Save or delete meta value / unset taxonomy.
				 */
        switch ($meta_type){
          // Taxonomy displayed in meta box
          case 'tax_select' :
          case 'tax_radio' : {
            $taxonomy = $meta_key;
            $term_id = (int) $new_val;
            //
            if (isset($new_val)){
              wp_set_object_terms($post_id, $term_id, $taxonomy);
            }
            break;
          }
          case 'tax_checkbox' : {
            $taxonomy = $meta_key;
            $terms_id = $new_val;
            //
            if (isset($new_val)){
              wp_set_object_terms($post_id, $terms_id, $taxonomy);
            }
            break;
          }
          // Text meta box
          case 'text':
          case 'select': {
            $this->delete_or_update_metavalue($post_id, $meta_key, $new_val, $old_val);
            break;
          }
          case 'checkbox': {
            $this->delete_or_update_metavalue($post_id, $meta_key, $new_val, $old_val);
            break;
          }
          case 'sortable':
          case 'sortable_select': {
            // unescape string
            $new_val = stripcslashes($new_val);
            $this->delete_or_update_metavalue($post_id, $meta_key, $new_val, $old_val);
            break;
          }
          // Meta box
          default:
            break;
        }

        /*
				 * Run post save callback function
				 */
        $this->postSave_callback($post_id, $meta_key, $new_val, $old_val);
      }
    }

    /**
     * Save or update meta value
     *
     * @param $post_id  int
     * @param $meta_key string
     * @param $new_val  mixed
     * @param $old_val
     */
    protected function delete_or_update_metavalue($post_id, $meta_key, $new_val, $old_val){
      if (isset($new_val)){
        if ('' === $new_val && $old_val){
          // delete old meta if new was not set
          $delete_status = delete_post_meta($post_id, $meta_key, $old_val);

        } elseif ($new_val != $old_val) {
          // update meta
          $update_status = update_post_meta($post_id, $meta_key, $new_val);
        }
      } elseif ($old_val) {
        // new value not set - delete the old one
        $delete_status = delete_post_meta($post_id, $meta_key, $old_val);
      }
    }

    protected function postSave_callback($post_id, $meta_key, $new_val, $old_val, $callback_args = null){
      $callback_args = empty($callback_args) ? $this->callback_args : $callback_args;

      foreach ($callback_args['meta_boxes'] as $metabox){
        $meta_type = $metabox['type'];

        if ($meta_type == '_group'){
          $this->postSave_callback($post_id, $meta_key, $new_val, $old_val, $metabox['settings']);
        }

        if (array_key_exists('settings', $metabox)){
          if (array_key_exists('callback_postSave', $metabox['settings'])){
            $callback_postSave = $metabox['settings']['callback_postSave'];
            $callback_postSave($post_id, $meta_key, $new_val, $old_val);
          }
        }

      }
    }

    //______________________________________________________________________________________________
    // GENERATE META BOX
    /**
     * @param $meta_box
     * @param $meta_key
     * @param $html_id
     * @param $meta_value
     * @param $settings
     * @param $post
     *
     * @return string
     */
    protected function generate_metabox_type__group($meta_box, $meta_key, $html_id, $meta_value, $settings, $post){

      $metabox_group_html = null;
      $metabox_group_html .= $this->meta_boxes_build($post, $settings['meta_boxes'], null, $meta_key);

      return $metabox_group_html;
    }

    /**
     * @param array $meta_box
     * @param       $meta_key
     * @param       $html_id
     * @param       $meta_value
     * @param       $settings
     *
     * @return string $metabox_html
     */
    protected function generate_metabox_type_text($meta_box, $meta_key, $html_id, $meta_value, $settings){
      $metabox_html = '
            <input type="text" name="' . esc_attr($meta_key) . '" value="' . esc_attr($meta_value) . '" class="regular-text" size="30" />
        ';

      return $metabox_html;
    }

    /**
     * @param array $meta_box
     * @param       $meta_key
     * @param       $html_id
     * @param       $meta_value
     * @param       $settings
     *
     * @return string $metabox_html
     */
    protected function generate_metabox_type_select($meta_box, $meta_key, $html_id, $meta_value, $settings){

      $options_html = '';
      foreach ($settings['select_options'] as $option){
        $options_html .= '<option value="' . $option['value'] . '" ' . selected($meta_value, $option['value'], false) . '>' . $option['label'] . '</option>';
      }

      $metabox_html = '
            <select name="' . esc_attr($meta_key) . '">
            ' . $options_html . '
            </select>
        ';

      return $metabox_html;
    }

    protected function generate_metabox_type_sortable($meta_box, $meta_key, $html_id, $meta_value, $settings){
      $sortable_items_html = '';
      foreach ($settings['items'] as $item){
        $html_id = $item['id'];
        $value = $item['value'];
        $contet_html = $item['html_content'];
        $sortable_items_html .= "
				<div class='sortable_item' style='margin-bottom: 10px;'>
						<input id='$html_id' type='text' name='$html_id' value='$value' style='display: none;'>
						$contet_html
				</div>
			";
      }

      $metabox_html = "
			<div class='sortable_container' style='background-color: lightgoldenrodyellow; border: 1px solid darkgray; padding: 10px;'>
				$sortable_items_html
			</div>
		";

      return $metabox_html;

    }

    protected function generate_metabox_type_sortableselect($meta_box, $meta_key, $html_id, $meta_value, $settings){
      $sortable_items_options_html = '';
      $sortable_items_selection_html = '';
      $sortable_items_selection_html_array = [];
      $sortable_items_selection_val = '';
      $sortable_items_selection_val_array = [];

      $selected_values_array = [];
      if (json_decode($meta_value)){
        $selected_values_array = json_decode($meta_value);
      }

      // sort array items by 'sort_string'
      $sortable_items = $settings['items'];
      foreach ($sortable_items as $key => $row){
        $sort_strings[$key] = $row['sort_string'];
      }
      $stat = array_multisort($sort_strings, SORT_ASC, $sortable_items);

      // crate sortable item html output
      for ($i = 0; $i < count($sortable_items); $i ++){
        $item = $sortable_items[$i];
        $html_id = $item['id'];
        $value = (string) $item['value'];
        $content_html = $item['html_content'];

        if (in_array($value, $selected_values_array)){
          // if item is selected
          $j = array_search($value, $selected_values_array, true);

          $sortable_items_selection_val_array[$j] = $value;
          $sortable_items_selection_html_array[$j] = "
						<div class='sortable_item' style='margin-bottom: 10px;'>
								<input class='sortable_input' id='$html_id' type='text' name='$html_id' value='$value' style='display: none;'>
								$content_html
						</div>
						";
        } else {
          // item is not selected yet
          $sortable_items_options_html .= "
						<div class='sortable_item' style='margin-bottom: 10px;'>
								<input class='sortable_input' id='$html_id' type='text' name='$html_id' value='$value' style='display: none;'>
								$content_html
						</div>
						";
        }
      }

      for ($k = 0; $k < count($sortable_items_selection_html_array); $k ++){
        $sortable_items_selection_val .= $sortable_items_selection_val_array[$k] . ' ';
        $sortable_items_selection_html .= $sortable_items_selection_html_array[$k];
      }

      $label_selection = $settings['label_selection'];
      $label_options = $settings['label_options'];
      $metabox_html = "	
			<div>
				<script type='text/javascript' src='http://aeroservis.dev/wp-content/themes/aeroservis/src/lib/Webmato/Wordpress/MetaBox/meta-box-sortable.js'></script>
				<!-- panel selected -->
				<div style='width: 49%; float: left'>
					<h4>$label_selection</h4>
					<input id='sortable_selection_result' name='$meta_key' type='text' value='$meta_value' style='display: none'/>
					<div id='sortable_container_selection' class='sortable_container' style='background-color: lightyellow; padding: 10px; border: 3px dashed #d3d3d3; border-radius: 10px;'>
						<!-- -->
						$sortable_items_selection_html
					</div>
				</div>

				<!-- panel options -->
				<div style='width: 49%; float: right'>
					<h4>$label_options</h4>
					<div id='sortable_container_options' class='sortable_container' style='background-color: lightyellow; padding: 10px; border: 3px dashed #d3d3d3; border-radius: 10px;'>
						$sortable_items_options_html
					</div>
				</div>

				<div style='clear: both'></div>
			</div>
		";

      return $metabox_html;

    }

    protected function generate_metabox_type_checkbox($meta_box, $meta_key, $html_id, $meta_value, $settings){
      $checkbox_html = '';
      $i = 0;
      foreach ($settings['checkbox_options'] as $checkbox_set){
        $checkbox_opt = $settings['checkbox_options'][$i];
        $label = $checkbox_opt['label'];
        $value = $checkbox_opt['value'];
        if (is_bool($value)){
          $value = ($value) ? 'true' : 'false'; //convert
        }

        if ( !array_key_exists('id', $checkbox_opt)){
          $checkbox_id = esc_attr('checkbox_' . $value);
        } else {
          $checkbox_id = esc_attr($checkbox_opt['id']);
        }
        if ( !array_key_exists('meta_key', $checkbox_opt)){
          $name = esc_attr($meta_key);
//          $name = esc_attr($meta_key.'__'.$checkbox_id);
        } else {
          $name = esc_attr($checkbox_opt['meta_key']);
        }

        /*
				 * Is checkbox checked?
				 */
        $checked_atr = '';
        if ($meta_value === (string) $value){
          $checked_atr = "checked='checked'";
        }


        $checkbox_html .= "
          <input id='$checkbox_id' type='checkbox' name='$name' value='$value' $checked_atr>
          <label for='$checkbox_id'></label> $label
          <br/>
        ";

        ++ $i;
      }

      return $checkbox_html;
    }

    /**
     * @param array $meta_box
     * @param       $meta_key
     * @param       $html_id
     * @param       $settings
     *
     * @internal param $post
     * @return string $metabox_html
     * @see      @link http://www.skyverge.com/blog/custom-post-type-with-image-uploads
     */
    protected function generate_metabox_type_gallery($meta_box, $meta_key, $html_id, $settings){
      $image_src = '';
      $image_id = get_post_meta(get_the_ID(), '_image_id', true);
      $image_src = wp_get_attachment_url($image_id);

      $post_args = [
        'screen'         => 'attachment',
        'post_status'    => 'inherit',
        'post_parent'    => get_the_ID(),
        'posts_per_page' => 1,
      ];
      $attachments = new \Wp_Query($post_args);


      if ( !empty($attachments->posts)){
        $tab = 'gallery';
      } else {
        $tab = 'type';
      }

      $upload_url = get_admin_url() . '/media-upload.php?post_id=' . get_the_ID() . '&width=670&height=400&tab=' . $tab;
      $metabox_html = '
            <iframe src="' . esc_url($upload_url) . '" width="100%" height="560">The Error!!!</iframe>
        ';

      return $metabox_html;
    }

    /**
     * @param $meta_box
     * @param $meta_key
     * @param $meta_value
     * @param $html_id
     * @param $settings
     *
     * @return string
     */
    protected function generate_metabox_type_wp_editor($meta_box, $meta_key, $html_id, $meta_value, $settings){
      $metabox_html = wp_editor($meta_value, $html_id, $settings) . '<br />';

      return $metabox_html;
    }

    protected function generate_metabox_type_tax_select($meta_box, $tax_key, $html_id, $tax_terms, $settings){
      $taxonomy = get_taxonomy($tax_key); // get taxonomy key name appropriate to this Taxonomy Meta box
      $post_terms = wp_get_object_terms(get_the_ID(), $tax_key); // get all taxonomy terms set of this post object

      /*
			 * set implicitly selected term of this post
			 */
      if ($post_terms){
        $selected = $post_terms[0]->term_id; // set first term of all terms

        // if more terms than 1, report to admin panel
        if (count($post_terms) > 1){
          $t = print_r($post_terms, true);
          $warn_html = "
                    <div class='warning' style='color:orangered;background-color:#d3d3d3; padding: 10px'>
                        <p>
                            <strong>Warning:</strong>
                            <br/>
                            In this post is set more than 1 taxonomy terms:
                        </p>
                        <pre>$t</pre>
                        <p>
                            Only one taxonomy term is allowed for this post. After post updating will be taxonomy terms set to selected value only.
                        </p>
                    </div>
                ";
        }
      } else {
        // no terms are selected for this post
        $selected = null;
      }


      /*
			 * Generate Options html tags for Select input
			 */
      $options_html = '';
      foreach ($tax_terms as $term){
        //$term_value = $taxonomy->hierarchical ? $term->term_id : $term->slug;
        $term_id = $term->term_id;

        $options_html .= '<option value="' . $term_id . '" ' . selected($selected, $term_id, false) . '">' . $term->name . '</option>';
      }
      unset($term);

      /*
			 * Generate complete Meta Box html
			 */
      $metabox_html = '
            <select name="' . esc_attr($tax_key) . '">
            ' . $options_html . '
            </select>
            <br/>
            <hr class="metabox-footer separator"/>
            <span class="description"><a href="' . get_bloginfo('url') . '/wp-admin/edit-tags.php?taxonomy=' . $tax_key . '">Manage ' . $taxonomy->label . '</a></span>
        ';
      if (isset($warn_html)){
        $metabox_html .= $warn_html;
      }

      return $metabox_html;
    }

    protected function generate_metabox_type_tax_checkbox($meta_box, $tax_key, $html_id, $tax_terms, $settings){
      $taxonomy = get_taxonomy($tax_key); // get taxonomy key name appropriate to this Taxonomy Meta box
      $post_obj_terms = wp_get_object_terms(get_the_ID(), $tax_key); // get all taxonomy terms set of this post object

      /*
			 * rebuild tax terms to hierarchical multidimensional array
			 */
      $tax_terms = $this->build_hierarchical_array($tax_terms, 'tax_terms');

      $metabox_html = $this->generate_metabox_type_tax_checkbox__unit($tax_key, $post_obj_terms, $tax_terms);
      $metabox_html .= '<span class="description"><a href="' . get_bloginfo('url') . '/wp-admin/edit-tags.php?taxonomy=' . $tax_key . '">Manage ' . $taxonomy->label . '</a></span>';

      return $metabox_html;
    }

    private function generate_metabox_type_tax_checkbox__unit($tax_key, $post_obj_terms, $taxonomy_terms, $level = 0){
      // indents for list levels
      $indent_html = '';
      for ($i = 0; $i < $level; $i ++){
        $indent_html .= '&mdash;&nbsp;';
      }

      $terms_checkbox_html = "<ul class='list level-$level'>";
      //
      $i = 1;
      foreach ($taxonomy_terms as $key => $tax_term){
        $name = $tax_term->name;
        $term_id = $tax_term->term_id;
        if ( !isset($tax_term->description) || '' == $tax_term->description){
          $term_desc = '';
        } else {
          $term_desc = '(' . $tax_term->description . ')';
        }


        /*
				 * set implicitly checked term property
				 */
        $checked = null; // term is not checked
        foreach ($post_obj_terms as $term_checked){
          // if current term is in array of checked terms
          if ($term_checked->term_id == $term_id){
            $checked = $term_checked->term_id; // set the checked property
            break;
          }
        }
        unset($term_checked);

        $form_name = esc_attr($tax_key . '-id_' . $term_id);
        $terms_checkbox_html .= "<li class='list-item level-$level'>" . $indent_html .
                                "<input type='checkbox' name='$form_name' value='$term_id' " . checked($checked, $term_id, false) . ">$name <span class='description'>$term_desc</span></br>";
        /*
				 * If term has any child repeat recursively this method.
				 */
        if (isset($tax_term->children)){
          $level ++;
          $terms_checkbox_html .= $this->generate_metabox_type_tax_checkbox__unit($tax_key, $post_obj_terms, $tax_term->children, $level);
          $level --;
        }


        $terms_checkbox_html .= "</li>";

        if (0 == $level && $i != count($taxonomy_terms)){
          $terms_checkbox_html .= '<hr/>';
        }
        $i ++;
      }
      unset($i);
      unset($key);
      unset($tax_term);
      //
      $terms_checkbox_html .= '</ul>';

      // add line break if not the last item
      if (0 < $level){
        //$terms_checkbox_html .= '<br/>';
      } else {
        $terms_checkbox_html .= '<hr class="metabox-footer separator"/>';
      }

      return $terms_checkbox_html;
    }

    /***********************************************************************************************
     *                                        HELPERS
     **********************************************************************************************/
    /**
     * @param $post_key
     *
     * @return bool
     */
    private function collect_posted_value($post_key){
      if (isset($_POST[$post_key])){
        return $_POST[$post_key];
      } else {
        return null;
      }
    }

    /**
     * Rebuild 1 dimensional array to hierarchical multidimensional array. The hierarchy style is
     * determined by $type and id predefined in this method.
     *
     * @param array  $src_array (Required) Default: none. Source array. Only 1 dimensional array
     *                          are supported
     * @param string $type      (Required) Default: none.
     * @param array  $new_array (Optional). Default: empty array. Multidimensional array where the
     *                          will be set the reorganized data from $src_array. Primarily for
     *                          recursive purposes of this method.
     * @param int    $level     (Optional). Default: 0. Do not set this parameter. It is set
     *                          automatically and for recursive purposes only.
     *
     * @return array $new_array
     * @throws \Exception
     */
    private function build_hierarchical_array($src_array, $type, $new_array = [], $level = 0){
      $level = (int) $level;

      switch ($type){
        case 'tax_terms' : {
          /*
					 * Set base 0 level of array for terms with property 'parent' == 0
					 */
          if (0 == $level){
            foreach ($src_array as $key => $term){
              if (0 == $term->parent){
                array_push($new_array, $term); // add term to $new_array
                unset($src_array[$key]); // delete term from $src_array
              }
            }
            unset($key);
            unset($term);
          }

          /*
					 * Sort rest terms by parent html_id
					 */
          foreach ($new_array as $parent){
            $children = null; // children of current parent

            // check all rest terms if they are child of current parent
            foreach ($src_array as $key => $term){

              if ($parent->term_id == $term->parent){
                $level ++;
                $children = isset($children) ? $children : [];
                unset($src_array[$key]); // delete term from $src_array

                /*
								 * Recursively call this method to sort and set children terms.
								 */
                $term_updated = $this->build_hierarchical_array($src_array, $type, [$term], $level);
                array_push($children, $term_updated); // add child to the $children array

                $level --;
              };
            }
            unset($key);
            unset($term);

            if ($children){
              $parent->children = $children; // set array of children ($children) to their parent
            }

          }
          unset($parent);

          /*
					 * Return rebuild array
					 */
          if (0 == $level){
            // for level = 0 return finished array
            //DEBUG: Helper::debug_print($new_array, 'Terms array', true);
            return $new_array;
          } else if (0 < $level){
            // return partial array
            return $new_array[0];
          } else {
            throw new \Exception("An error occurred in method build_hierarchical_array for array '$type' type!");
          }
        }
        default:
          throw new \Exception ("Error! This array type: '$type' is not supported!");
      }
      //end switch
    }

    /**
     * Replace white spaces with '_' and make lowercase.
     *
     * @param $str string
     *
     * @return mixed|string
     */
    private function normalize_str($str){
      $str = preg_replace('/\s+/', '_', $str); // replace white spaces
      $str = strtolower($str); // to lowercase
      return $str;
    }


    /***********************************************************************************************
     *                                        GET/SET
     **********************************************************************************************/

    /**
     * @param $id string
     */
    public function setId($id){
      // set default $meta__html_id__box
      if ($id === null){
        $id = $this->WEBMATO_PREFIX_META . $this->meta_IdName_base . '__box'; //html_id: twp_meta__ph_gallery__template__box
      }
      $this->id = $id;
    }

    /**
     * @param array|string $screen
     */
    public function setScreen($screen = null){
      if ($screen){
        if ( !is_array($screen)){
          $screen = [$screen];
        }
        $this->screen = $screen;
      }
    }

    /**
     * @param string $context  (Optional) (Default:'side')
     *                         The context within the screen where the boxes should display.
     *                         Available contexts vary from screen to screen. Post edit screen
     *                         contexts include 'normal', 'side', and 'advanced'. Comments screen
     *                         contexts include 'normal' and 'side'. Menus meta boxes (accordion
     *                         sections) all use the 'side' context.
     * @param string $priority (Optional) (Default:'default')
     *                         The priority within the context where the boxes should show ('high',
     *                         'low').
     *
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $context
     * @see @link https://developer.wordpress.org/reference/functions/add_meta_box/#parameters ->
     *      $priority
     */
    public function setAdminUiArgs($context = 'side', $priority = 'default'){
      $this->context = $context;
      $this->priority = $priority;
    }

    /**
     * @param array $callback
     */
    public function setCallback($callback = null){
      if ([] === $callback || null === $callback){
        $this->callback = [$this, 'meta_box_callback'];
      } else {
        $this->callback = $callback;
      }
    }

    /**
     * @param array $callback_args
     */
    public function setCallbackArgs($callback_args = null){
      // add default callback getPosts_args values if missing
      foreach ($callback_args['meta_boxes'] as &$one_cb_args){
        //add 'key' value
        if ( !array_key_exists('key', $one_cb_args) && $one_cb_args['type'] != '_group'){
          $one_cb_args['key'] = $this->WEBMATO_PREFIX_META . $this->meta_IdName_base; //twp_meta__ph_gallery__template
        }

        //add 'id' value
        if ( !array_key_exists('id', $one_cb_args)){
          $one_cb_args['id'] = $this->WEBMATO_PREFIX_META . $this->meta_IdName_base . '__form';  //html_id: twp_meta__ph_gallery__template__form
        }
      }

      if ([] === $callback_args || null === $callback_args){
        $this->callback_args = [];
      } else {
        $this->callback_args = $callback_args;
      }
    }

  }
