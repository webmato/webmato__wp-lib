<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\MetaBox;

  /**
   * Class MetaBoxCallbackBase
   *
   * @package Webmato\Wordpress\MetaBox
   */
  class MetaBoxCallbackBase {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    /** @var string $callback_f Name of the callback function. Render the meta box in the WP admin. */
    public $callback_f;

    /** @var array $callback_args Argument for the call back function. */
    public $callback_args;

    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    /**
     * @param array $callback_args
     */
    function __construct($callback_args = []){
      $this->callback_f = [$this, 'callback'];

      if ([] !== $callback_args && is_array($callback_args)){
        $this->callback_args = $callback_args;
      } else {
        $this->setCallbackArgs();
      }
    }


    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/
    //______________________________________________________________________________________________
    // ADMIN
    /**
     * @param $post
     * @param $args
     */
    public function callback_function($post, $args){
      echo($args['getPosts_args']['text']);
    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/


    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/
    /**
     * @internal param array $callback_args
     */
    protected function setCallbackArgs(){
      $this->callback_args = [
        // add arguments
        'text' => 'Hello Meta Box',
      ];
    }
  }
