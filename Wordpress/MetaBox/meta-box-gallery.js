/*
 * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
 */


jQuery(document).ready(($) => {
  /*************************************************************************************************
   *                                      Common Variables
   ************************************************************************************************/


  /*************************************************************************************************
   *                                          Methods
   ************************************************************************************************/


  /*************************************************************************************************
   *                                    Event Handlers
   ************************************************************************************************/


  /*************************************************************************************************
   *                                     Controllers
   ************************************************************************************************/


  /*************************************************************************************************
   *                                        Init
   ************************************************************************************************/

  // TODO: rebuild code

  alert('jQuery(document).ready(function($)');
  // save the send_to_editor handler function
  window.send_to_editor_default = window.send_to_editor;

  // BTN - Set Book Image
  $('#set-book-image').click(() => {
    // alert('$('#set-book-image').click(function()');
    // replace the default send_to_editor handler function with our own
    window.send_to_editor = window.attach_image;
    tb_show('', 'media-upload.php?post_id=<?php echo $post->ID ?>&amp;type=image&amp;TB_iframe=true');
    return false;
  });

  // BTN - Remove Book image
  $('#remove-book-image').click(() => {
    $('#upload_image_id').val('');
    $('img').attr('src', '');
    $(this).hide();
    return false;
  });

  // handler function which is invoked after the user selects an image from the gallery popup.
  // this function displays the image and sets the id so it can be persisted to the post meta
  window.attach_image = function (html) {
    alert('window.attach_image = function(html)');
    // turn the returned image html into a hidden image element so we
    // can easily pull the relevant attributes we need
    $('body').append(`<div id="temp_image">${html}</div>`);

    const img = $('#temp_image').find('img');

    const imgurl = img.attr('src');
    const imgclass = img.attr('class');
    const imgid = parseInt(imgclass.replace(/\D/g, ''), 10);

    $('#upload_image_id').val(imgid);
    $('#remove-book-image').show();

    $('img#book_image').attr('src', imgurl);
    try {
      tb_remove();
    } catch (e) {
      //
    }

    $('#temp_image').remove();

    // restore the send_to_editor handler function
    window.send_to_editor = window.send_to_editor_default;
  };
});
