<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  // TODO: Rebuild all code


  $prefix = 'WEBMATO' . '_';

// ENQUEUE SCRIPTS FOR META BOXES ////////////////////////////////////////////////////////////////
  wp_enqueue_script('media-upload'); //todo zmenit umisteni vkladani skriptu na lepsi a logictejsi misto


// META BOXES ARGUMENTS ////////////////////////////////////////////////////////////////
  $meta_box_custom_id = $prefix . 'meta_template';
  $meta_box_custom_title = 'Custom Meta Boxes - Example';
  $meta_box_custom_args = [
    'priority' => 'default',
    'context'  => 'advanced',
  ];

  /**
   * DOC
   * type =>
   *      text:
   *      select:
   *      gallery:
   *
   *      _repeat: array[meta_box_args] - Group of several meta boxes or one meta box
   *      _group: array[
   *          'meta_boxes' => array[meta_box_args] - Group of several meta boxes. Especially useful
   *          inside _repeat type (example: repeated group of text, image, select...)
   *      ]
   *
   *
   * $meta_box_args = [
   *  'meta_boxes' => [
   *      [
   *          'type'      => string (Required) Meta box type <'text', 'select', 'gallery',
   *          'wp_editor', '_group', '_repeat'>.
   *          'key'       => string (Required | Optional for '_group') Name of meta key. If set for
   *          '_group', then meta values from 'meta_boxes_group' array will be serialized and
   *          stored under one meta key.
   *          'id'        => string (Optional) CSS id.
   *          'label'     => string (Optional) Label displayed in Admin panel by meta box.
   *          'desc'      => string (Optional) Text description.
   *          'settings'  => array  (Optional) Array of settings by meta box 'type'.
   *      ],
   *
   *      ...
   *
   *      [
   *          'type'              => '_group',
   *          'id'                => 'text1_text2',
   *          'label'             => 'Group of text inputs'
   *          'settings' => [
   *              'meta_boxes_group' => [ // array of meta boxes arguments
   *                  [type => 'text', id => 'text1'],
   *                  [type => 'text', id => 'text2'],
   *              ]
   *          ]
   *      ]
   *  ],
   *  'other_custom_args' => [...]
   * ];
   *
   */

  $meta_setting = [

    // Select Meta Box
    [
      'type'     => 'select',
      'key'      => $prefix . 'selected',
      'id'       => $prefix . 'meta_select_template',
      'label'    => 'Meta Box Type: "select"',
      'desc'     => 'Description for select Meta Box input',
      'settings' => [
        'select_options' => [ // array of options for html Select box
          '1' => [ // array key needs to be the same as the option value
            'label' => 'Select option 1', // text displayed as the option
            'value' => 'select_option_1' // value stored for the option
          ],
          '2' => [
            'label' => 'Select option 2',
            'value' => 'select_option_2',
          ],
          '3' => [
            'label' => 'Select option 3',
            'value' => 'select_option_3',
          ],
        ],
      ],
    ],

    // Group Meta Box
    [
      'type'     => '_group',
      //'key'               => $prefix .'text1_text2',
      'id'       => $prefix . 'meta_text1_text2',
      'label'    => 'Meta Box Type: "_group"',
      'settings' => [
        'meta_boxes_group' => [
          [
            'type'  => 'text',
            'key'   => $prefix . 'text_1',
            'id'    => $prefix . 'meta_text_1',
            'label' => 'Text Input 1',
            'desc'  => 'Description for Text Input 1',
          ],
          [
            'type'  => 'text',
            'key'   => $prefix . 'text_2',
            'id'    => $prefix . 'meta_text_2',
            'label' => 'Text Input 2',
            'desc'  => 'Description for Text Input 2',
          ],
          [
            'type'  => 'text',
            'key'   => $prefix . 'text_3',
            'id'    => $prefix . 'meta_text_3',
            'label' => 'Text Input 3',
            'desc'  => 'Description for Text Input 3',
          ],
          [
            'type'  => 'text',
            'key'   => $prefix . 'text_4',
            'id'    => $prefix . 'meta_text_4',
            'label' => 'Text Input 4',
            'desc'  => 'Description for Text Input 4',
          ],
        ],
      ],
    ]

    //

  ];
  $meta_box_custom_callback_args = [
    'meta_boxes' => $meta_setting,
  ];

// REGISTER META BOXES ////////////////////////////////////////////////////////////////
  $meta_template = new \Webmato\Wordpress\MetaBox\MetaBoxBase(
    $meta_box_custom_id
    , $meta_box_custom_title
    , 'page'
    , $meta_box_custom_args
    , $meta_box_custom_callback_args
  );
