<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\PostTypes;

  use Webmato\Wordpress\Helper;
  use Webmato\Wordpress\MetaBox\MetaBoxBase;
  use Webmato\Wordpress\Taxonomy\TaxonomyBase;

  /**
   * Class PostTypeBase
   *
   * @package Webmato\Wordpress\PostTypes
   */
  class PostTypeBase {
    /***********************************************************************************************
     *                                        VARS
     **********************************************************************************************/
    protected $names_key = ['name_singular', 'name_plural', 'slug'];
    protected $auto_init;

    /**
     * @var string $domain
     */
    protected $domain;

    /**
     * @var string $post_type_name_base
     */
    protected $post_type_name_base;

    /**
     * @var $post_type_name
     */
    protected $post_type_name;

    /**
     * @var array $names
     */
    protected $post_names;

    /**
     * @var array $post_args
     */
    protected $post_args;

    /**
     * @var array $post_args_custom
     */
    protected $post_args_custom;

    /**
     * @var array $post_args_default__supports
     */
    protected $post_args_default__supports = [
      'title',
      'editor',
      'revision',
      'thumbnail',
      'author',
    ];

    /**
     * @var array Default ['category', 'post_tag ']. Default array of taxonomies to register
     */
    protected $post_args_default__taxonomies = ['category', 'post_tag'];

    /**
     * @var array $post_args_default
     */
    protected $post_args_default;

    // $post_labels_default
    /**
     * @var array $post_labels_default
     */
    protected $post_labels_default;

    /**
     * @var array $metaBoxes | Array of Meta_Box_Base objects
     */
    protected $metaBoxes = [];

    /**
     * @var string $WEBMATO_PREFIX_POST
     */
    private $WEBMATO_PREFIX_POST;


    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    /**
     * Create Custom Post Type
     *
     * Constructor register new custom post type in WP.
     *
     * @param string $post_type__base_    Required)
     *                                    Post type name.
     * @param array  $post_names          (Optional)
     *                                    An array of names.
     *                                    <code>
     *                                    array $names =
     *                                    [
     *                                    'name_singular' => string // (Optional) (Default: auto
     *                                    generated) Singular name of post type used as singular in
     *                                    post labels. Leads with uppercase.
     *                                    'name_plural' => string // (Optional) (Default: auto
     *                                    generated) Plural name of post type used as plural in
     *                                    post labels. Leads with uppercase.
     *                                    'slug' => string // (Optional) (Default: auto generated)
     *                                    Slug name. Lowercase, a-z0-9 and hyphen '-'. No spaces
     *                                    and underscores.
     *                                    ]
     *                                    </code>
     * @param array  $post_args_custom    (Optional) (Default: [])
     *                                    Arguments for register post
     *                                    type - same as for WP method register_post_type()
     * @param string $domain              (Optional) (Default: '')
     *                                    Domain used in WP __() method For WP language
     *                                    localization.
     * @param bool   $auto_init           (Optional) (Default: true)
     *                                    Post construct automatically init Post Type class -
     *                                    register register_post_type etc.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type#Arguments
     *
     * @uses __
     * @uses register_post_type
     * @uses Helper::array_merge_custom_to_default
     */
    public function __construct(
      $post_type__base_,
      $post_names = [],
      $post_args_custom = [],
      $domain = '',
      $auto_init = true
    ){
      if (defined('WEBMATO_PREFIX_POST')){
        $this->WEBMATO_PREFIX_POST = WEBMATO_PREFIX_POST;
      } else {
        $this->WEBMATO_PREFIX_POST = '';
      }
      // set properties
      $this->setPostTypeNameBase($post_type__base_);
      $this->setPostTypeName($this->WEBMATO_PREFIX_POST . $post_type__base_);
      $this->setDomain($domain);
      $this->setPostNames($this->post_type_name, $post_names);
      $this->setPostArgsCustom($post_args_custom);
      $this->setPostArgsDefault($this->post_names);
      $this->setPostArgs($this->post_args_default, $this->post_args_custom);

      $this->auto_init = $auto_init;

      // init if $auto_init === true (default)
      if ($auto_init){
        add_action('init', [&$this, 'init']);
      }
    }

    /***********************************************************************************************
     *                                        METHODS
     **********************************************************************************************/
    //______________________________________________________________________________________________
    // GENERAL

    /**
     * Init the register process of this post type
     */
    public
    function init(){
      $this->reg_post_type($this->post_type_name, $this->post_args);

      //if (is_admin()) {
      //    add_action('admin_init', array(&$this, 'init_admin'));
      //}
    }

    public
    function init_admin(){
      //add_action('add_meta_boxes', array(&$this, 'meta_boxes'));
      //add_action('save_post', array(&$this, 'meta_boxes_save'), 1, 2);
    }

    /**
     * @param string $post_type_name
     * @param array  $post_args
     *
     * @return object|\WP_Error
     * @see  __construct
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     * @uses reg_post_type
     */
    protected
    function reg_post_type($post_type_name, $post_args
    ){
      $ret = register_post_type($post_type_name, $post_args);

      return $ret;
    }

    /**
     * @param array $taxonomies (Required) (Default: None)
     *                          Array of taxonomy arrays. To register taxonomy uses the
     *                          Taxonomy_Base class.
     *                          <code>
     *                          array $taxonomies =
     *                          [
     *                          [
     *                          'tax_key' => string // (required) (Default: None) Taxonomy key
     *                          name.
     *                          'names' => array of strings // (Optional) (Default: []) An array of
     *                          names
     *                          [
     *                          'name_singular' => string // (Optional) (Default: 'Gallery')
     *                          Singular name of post type used as singular in post labels. Leads
     *                          with uppercase.
     *                          'name_plural' => string // (Optional) (Default: 'Galleries') Plural
     *                          name of post type used as plural in post labels. Leads with
     *                          uppercase.
     *                          'slug' => string // (Optional) (Default: auto generated) Slug name.
     *                          Lowercase, a-z0-9 and hyphen '-'. No spaces and underscores.
     *                          ]
     *                          'getPosts_args' => array // (Optional) (Default: []) Arguments for register
     *                          post type - same as for WP method reg_taxonomy().
     *                          ],
     *                          [...]
     *                          ]
     *                          </code>
     *
     * @return array Taxonomy_Base
     * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
     */
    public
    function addTaxonomyToPostType($taxonomies
    ){
      $ts = [];
      foreach ($taxonomies as $key => $tax){
        if ( !array_key_exists('names', $tax)){
          $tax['names'] = [];
        }
        if ( !array_key_exists('getPosts_args', $tax)){
          $tax['getPosts_args'] = [];
        }
        array_push($ts, new TaxonomyBase($tax['tax_key'], [$this->post_type_name], $tax['names'], $tax['getPosts_args']));
      }

      return $ts;
    }

    /**
     * Override this method and init register process of all default meta boxes from here.
     *
     * @return array [Meta_Box_Base] | Array of Meta_Box_Base - registered meta boxes.
     */
    public function addDefaultMetaBoxes(){
      // your this->addMetaBox_XYZ(); method
      return $this->metaBoxes;
    }

    /**
     * @param string $meta_base_name     (Required)
     * @param string $meta_title         (Required)
     *                                   Html title of this meta box in the admin panel.
     * @param array  $meta_callback_args (Required)
     *                                   Arguments passed into callback function.
     *                                   Callback arguments are important for generating the meta
     *                                   boxes. Content of getPosts_args depends on meta box type.
     *                                   <code>
     *                                   array $meta_callback_args =
     *                                   [
     *                                   'meta_boxes' =>
     *                                   [
     *                                   'type'   => string  // (Required) meta box type
     *                                   'key'    => string  // (Optional: auto generated) meta key
     *                                   name
     *                                   'id'     => string  // (Optional: auto generated) CSS id
     *                                   'label'  => string  // (Optional)
     *                                   'desc'   => string  // (Optional: none) text description
     *                                   ],
     *                                   'custom' => [],
     *                                   'other_custom' => []
     *                                   ]
     *                                   </code>
     * @param array  $meta_args          (Optional)
     *                                   <code>
     *                                   $meta_args =
     *                                   [
     *                                   'priority'  => string   // (Optional) (Default: 'default')
     *                                   ('high' | 'core' | 'default' | 'low') The priority within
     *                                   the context where the boxes should shown.
     *                                   'context'   => string   // (Optional) (Default:
     *                                   'advanced') ('normal' | 'advanced' | 'side') The part of
     *                                   the page where the edit screen section should be shown.
     *                                   ]
     *                                   </code>
     * @param string $meta__html_id__box (Optional) (Default: auto generated) SS ID used in html
     *                                   for this meta box.
     *
     * @return MetaBoxBase
     *
     * @link http://codex.wordpress.org/Function_Reference/add_meta_box
     * @link http://codex.wordpress.org/Function_Reference/add_meta_box#Parameters
     * @link http://codex.wordpress.org/Function_Reference/add_meta_box#Callback_args
     */
    public function addMetaBox(
      $meta_base_name,
      $meta_title,
      array $meta_callback_args = [],
      array $meta_args = [
        'priority' => 'default',
        'context'  => 'advanced',
      ],
      $meta__html_id__box = null
    ){
      $meta_name = $this->getPostTypeNameBase() . '__' . $meta_base_name;

      $metaBox = new MetaBoxBase(
        $meta_name,
        $this->post_type_name,
        $meta__html_id__box,
        $meta_title,
        $meta_args,
        $meta_callback_args
      );

      array_push($this->metaBoxes, $metaBox);

      return $metaBox;
    }

    /***********************************************************************************************
     *                                        GET/SET
     **********************************************************************************************/

    /**
     * @param string $domain
     */
    protected function setDomain($domain){
      $this->domain = $domain;
    }

    /**
     * @return string $domain
     */
    public function getDomain(){
      return $this->domain;
    }

    /**
     * @return string
     */
    public function getPostTypeNameBase(){
      return $this->post_type_name_base;
    }

    /**
     * @param string $post_type_name_base
     */
    public function setPostTypeNameBase($post_type_name_base){
      $this->post_type_name_base = $post_type_name_base;
    }

    /**
     * @param string $post_type_name
     *
     * @throws \Exception
     */
    protected function setPostTypeName($post_type_name){
      if (strlen($post_type_name) > 20){
        throw new \Exception(
          "Post type names must be between 1 and 20 characters in length. " .
          "\nYou have passed post type name: \"$post_type_name\" and lenght of this name is " . strlen($post_type_name) . "." .
          "\nSee https://developer.wordpress.org/reference/functions/register_post_type/#parameters");
      }


      $this->post_type_name = $post_type_name;
    }

    /**
     * @return string
     */
    public function getPostTypeName(){
      return $this->post_type_name;
    }

    /**
     * @param $post_args_default
     * @param $post_args_custom
     *
     * @internal param array $post_args
     */
    protected function setPostArgs($post_args_default, $post_args_custom){
      $this->post_args = Helper::array_merge_custom_to_default($post_args_default, $post_args_custom);
    }

    /**
     * @return array
     */
    public function getPostArgs(){
      return $this->post_args;
    }

    /**
     * @param $post_names
     *
     * @internal param mixed $post_labels_default
     */
    protected function setPostLabelsDefault($post_names){
      $domain = $this->domain;

      $post_labels_default = [
        // General name for the post type, usually plural. The same as, and overridden by $post_type_object->label
        'name'               => __($post_names['name_plural'], $domain),

        // Name for one object of this post type. Defaults to value of name
        'name_singular'      => __($post_names['name_singular'], $domain),

        // The menu name text. This string is the name to give menu items. Defaults to value of name
        'menu_name'          => __($post_names['name_plural'], $domain),

        // The all items text used in the menu. Default is the Name label
        'all_items'          => __('All ' . $post_names['name_plural'], $domain),

        // The add new text. The default is Add New for both hierarchical and non-hierarchical types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
        'add_new'            => __('Add New ' . $post_names['name_singular'], $domain),

        // The add new item text. Default is Add New Post/Add New Page
        'add_new_item'       => __('Add New ' . $post_names['name_singular'], $domain),

        // The edit item text. Default is Edit Post/Edit Page
        'edit_item'          => __('Edit ' . $post_names['name_singular'], $domain),

        // The new item text. Default is New Post/New Page
        'new_item'           => __('New ' . $post_names['name_singular'], $domain),

        // The view item text. Default is View Post/View Page
        'view_item'          => __('View ' . $post_names['name_singular'], $domain),

        // The search items text. Default is Search Posts/Search Pages
        'search_items'       => __('Search ' . $post_names['name_plural'], $domain),

        // The not found text. Default is No posts found/No pages found
        'not_found'          => __('No matching ' . strtolower($post_names['name_plural']) . ' found', $domain),

        // The not found in trash text. Default is No posts found in Trash/No pages found in Trash
        'not_found_in_trash' => __('No ' . strtolower($post_names['name_plural']) . ' found in Trash', $domain),

        // The parent text. This string isn't used on non-hierarchical types. In hierarchical ones the default is Parent
        'parent_item_colon'  => __('Parent ' . $post_names['name_singular'], $domain),
      ];

      $this->post_labels_default = $post_labels_default;

    }

    /**
     * @return mixed
     */
    public function getPostLabelsDefault(){
      return $this->post_labels_default;
    }

    /**
     * @param $post_names
     *
     * @internal param array $post_args_default
     */
    protected function setPostArgsDefault($post_names){
      $this->setPostLabelsDefault($post_names);

      $this->post_args_default = [
        'label'      => $post_names['name_plural'],
        'labels'     => $this->getPostLabelsDefault(),
        'public'     => true,
        'supports'   => $this->post_args_default__supports,
        'taxonomies' => $this->post_args_default__taxonomies,
        'rewrite'    => ['slug' => $post_names['slug']],
      ];
    }

    /**
     * @return array
     */
    public function getPostArgsDefault(){
      return $this->post_args_default;
    }

    /**
     * Parse or generate plural, singular and slug names
     *
     * @param string $base_name (Require)
     * @param array  $names     (Optional) (Default: [])
     *                          <code>
     *                          array $names =
     *                          [
     *                          'name_singular' => string // (Optional) (Default: auto generated)
     *                          Singular name of post type used as singular in post labels. Leads
     *                          with uppercase.
     *                          'name_plural' => string // (Optional) (Default: auto generated)
     *                          Plural name of post type used as plural in post labels. Leads with
     *                          uppercase.
     *                          'slug' => string // (Optional) (Default: auto generated) Slug name.
     *                          Lowercase, a-z0-9 and hyphen '-'. No spaces and underscores.
     *                          ]
     *                          </code>
     *
     * @throws \Exception Only a string in $base_name and an array of strings in $names is
     *                    accepted.
     *
     * @see     Helper::str_wp_normal
     * @see     __construct
     * @see     post_names
     */
    protected function setPostNames($base_name, $names = []){
      $domain = $this->domain;

      if (is_string($base_name) && is_array($names)){
        //If is name doesn't exists
        foreach ($this->names_key as $key => $val){
          if ( !array_key_exists($val, $names) || '' === $names[$val]){
            // if singular name exists but plural not
            if ('name_plural' === $val && ($names['name_singular'] && '' !== $names['name_singular'])){
              $names[$val] = $names['name_singular'];
            } else {
              $names[$val] = $base_name;
            }
          }
        }

        $names['name_singular'] = ucwords(__($names['name_singular'], $domain));
        $names['name_plural'] = ucwords(__($names['name_plural'], $domain));
        $names['slug'] = Helper::str_wp_normal($names['slug']);

      } else {
        throw new \Exception('Only a string in $base_name and an array of strings in $names is accepted.');
      }

      $this->post_names = $names;
    }

    /**
     * @return array
     */
    public function getPostNames(){
      return $this->post_names;
    }

    /**
     * @param array $post_args_custom
     */
    protected function setPostArgsCustom($post_args_custom){
      $this->post_args_custom = $post_args_custom;
    }

    /**
     * @return array
     */
    public function getPostArgsCustom(){
      return $this->post_args_custom;
    }

  }
