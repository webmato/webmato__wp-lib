<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\PostTypes;


  use Webmato\Wordpress\MetaBox\MetaBoxBase;

  /**
   * Gallery Post Type
   *
   * Basic Gallery Post Type based on Post Type Base
   *
   * Class Post_Type_Gallery
   *
   * @package Webmato\Wordpress\PostTypes
   */
  class GalleryPostType extends PostTypeBase {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    protected $post_args_default__supports = [
      'title',
      'excerpt',
      'revision',
      'thumbnail',
      'author',
      'comments',
    ];


    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    /**
     * GalleryPostType constructor.
     *
     * @param array  $post_args_custom
     * @param array  $post_names
     * @param string $domain
     * @param bool   $auto_init
     * @param string $postType_base_name - Lowercase
     */
    public function __construct(
      $post_args_custom = ['menu_icon' => null],
      $post_names = ['name_singular' => 'Gallery', 'name_plural' => 'Galleries'],
      $domain = '',
      $auto_init = true,
      $postType_base_name = 'gallery'
    ){
      if ($post_names == null){
        $post_names = ['name_singular' => 'Gallery', 'name_plural' => 'Galleries'];
      }

      parent::__construct(
        $postType_base_name,
        $post_names,
        $post_args_custom,
        $domain,
        $auto_init
      );

      $this->addColumnsToAdminPanel($postType_base_name);
    }

    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/

    //______________________________________________________________________________________________
    // GENERAL
    /**
     * Add columns to admin panel for gallery post type
     *
     * @param string $post_type__base_name (Required)
     *                                     Post type key name of post type to add this columns
     *
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns
     * @link https://codex.wordpress.org/Plugin_API/Action_Reference/manage_$post_type_posts_custom_column
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$post_type_posts_columns
     */
    protected function addColumnsToAdminPanel($post_type__base_name){
      /**
       * Custom columns for admin panel
       */
      add_action('admin_init', function() use ($post_type__base_name){

        /*
				 * Add / remove columns
				 */
        add_filter(
        //'manage_edit-th_post__post_gallery_columns',
          'manage_edit-' . $post_type__base_name . '_columns',
          function($gallery_columns){

            //$new_columns['cb'] = '<input type="checkbox" />';

            unset($gallery_columns['author']);

            //$gallery_columns['id'] = __('ID');
            //$new_columns['title'] = _x('Gallery Name', 'column name');
            $gallery_columns['images'] = __('Images');
            //$new_columns['author'] = __('Author');

            $gallery_columns['thumbnail'] = __('Thumbnail');
            //$new_columns['categories'] = __('Categories');
            //$new_columns['tags'] = __('Tags');

            //$new_columns['date'] = _x('Date', 'column name');

            return $gallery_columns;
          }
        );

        /*
				 * Manage content of this custom columns
				 */
        add_action(
          'manage_' . $post_type__base_name . '_posts_custom_column',
          function($column_name, $id){
            global $wpdb;

            switch ($column_name){
              case 'id' :
                echo $id;
                break;
              case 'thumbnail' :
                if (has_post_thumbnail($id)){
                  $thumbnail_id = get_post_thumbnail_id($id);
                  $img = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
                  $imgsrc = $img[0];
                  echo("<img class='' src='$imgsrc' style='width:100px; height: 100px; border: 1px solid'/>");
                } else {
                  echo("<p>Not set</p>");
                }

                break;
              case 'images' :
                $num_images = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_parent = {$id};", $id));
                echo $num_images;
                break;
              default:
                break;
            }
          }
          , 10
          , 2
        );
      });
    }

    //______________________________________________________________________________________________
    // META BOXES
    public function addDefaultMetaBoxes(){
      // Add meta box Images Set. Set the images of current gallery.
      $this->addMetaBox_imagesSet();
    }

    /**
     * Create meta box (meta box type 'select') and add select options contains template files
     *
     * @param array $options (Required)
     *                       Array of html select options arrays. This contains gallery template
     *                       options.
     *                       <code>
     *                       array $options =
     *                       [
     *                       [
     *                       'label' =>  string // (Required) option html tag text. Text label of
     *                       the template file.
     *                       'value' =>  string // (Required) option html tag value. File name of
     *                       the gallery template.
     *                       ],
     *                       [
     *                       ...
     *                       ]
     *                       ]
     *                       </code>
     *
     * @return MetaBoxBase
     *
     * @see  add_meta_box
     * @see  addMetaBox
     * @link https://codex.wordpress.org/Function_Reference/add_meta_box
     * @link http://www.w3schools.com/tags/tag_option.asp
     */
    public function addMetaBox_templateOptions(array $options){
      $meta__pt__gallery__template__callback_args = [
        'meta_boxes' => [
          [
            'type'     => 'select',
            'desc'     => 'Select template for this Gallery page.',
            'settings' => [
              'select_options' => $options,
            ],
          ],
        ],
      ];

      return parent::addMetaBox('template', 'Gallery Template', $meta__pt__gallery__template__callback_args);
    }

    /**
     * Create meta box of gallery type for uploading images to the gallery post type.
     *
     * @return MetaBoxBase
     * @see  add_meta_box
     * @see  addMetaBox
     * @link https://codex.wordpress.org/Function_Reference/add_meta_box
     */
    public function addMetaBox_imagesSet(){
      $meta__pt__gallery__images__callback_args = [
        'meta_boxes' => [
          [
            'type' => 'gallery',
            'desc' => 'Upload from computer some images and set featured picture for this gallery.',
          ],
        ],
      ];

      return parent::addMetaBox('images', 'Select Images', $meta__pt__gallery__images__callback_args);
    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/


    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/
  }
