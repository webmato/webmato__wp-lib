<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  /**
   * Created by PhpStorm.
   * User: Tomas
   * Date: 26. 6. 2015
   * Time: 16:31
   */

  namespace Webmato\Wordpress\PostTypes;


  use Webmato\Wordpress\MetaBox\MetaBoxBase;
  use WP_Query;

  class GalleryCollectionPostType extends PostTypeBase {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    /** @var string $gallery_postType_keyName */
    protected $gallery_postType_keyName;

    /** @var string $WEBMATO_PREFIX_META */
    private $WEBMATO_PREFIX_META;

    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    /**
     * Create custom Gallery Collection post type
     *
     * Constructor register new custom post type in WP.
     *
     * @param string       $gallery_postType_key
     *
     * @param array|string $post_args_custom           (Optional) (Default: [])
     *                                                 Arguments for register post type - same as
     *                                                 for WP method register_post_type().
     *
     * @param array        $post_names                 (Optional)
     *                                                 An array of names
     *                                                 <code>
     *                                                 array $post_names =
     *                                                 [
     *                                                 'name_singular' => string // (Optional)
     *                                                 (Default: 'Gallery Collection') Singular
     *                                                 name of post type used as singular in post
     *                                                 labels. Leads with uppercase.
     *                                                 'name_plural' => string // (Optional)
     *                                                 (Default: 'Gallery Collections') Plural name
     *                                                 of post type used as plural in post labels.
     *                                                 Leads with uppercase.
     *                                                 'slug' => string // (Optional) (Default:
     *                                                 auto generated) Slug name. Lowercase, a-z0-9
     *                                                 and hyphen '-'. No spaces and underscores.
     *                                                 ]
     *                                                 </code>
     *
     * @param string       $domain                     (Optional) (Default: "")
     *                                                 Domain used in WP __() method for WP
     *                                                 language localization.
     *
     * @param bool         $auto_init                  (Optional) (Default: true)
     *                                                 Post construct automatically init Post Type
     *                                                 class - register register_post_type etc.
     *
     * @param string       $postType_base_name         (Required)
     *                                                 Post type name.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type#Arguments
     *
     * @uses __
     * @uses register_post_type
     * @uses Helper::array_merge_custom_to_default
     */
    function __construct(
      $gallery_postType_key,
      $post_args_custom = ['menu_icon' => null],
      $post_names = [
        'name_singular' => 'Gallery Collection',
        'name_plural'   => 'Gallery Collections',
      ],
      $domain = '',
      $auto_init = true,
      $postType_base_name = 'gal_col'
    ){
      if (defined('WEBMATO_PREFIX_META')){
        $this->WEBMATO_PREFIX_META = WEBMATO_PREFIX_META;
      } else {
        $this->WEBMATO_PREFIX_META = '';
      }
      //
      $this->gallery_postType_keyName = $gallery_postType_key;
      if ($post_names == null){
        $post_names = [
          'name_singular' => 'Gallery Collection',
          'name_plural'   => 'Gallery Collections',
        ];
      }

      parent::__construct(
        $postType_base_name,
        $post_names,
        $post_args_custom,
        $domain,
        $auto_init
      );

      $this->addColumnsToAdminPanel($postType_base_name);
    }


    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/
    //______________________________________________________________________________________________
    // GENERAL

    /**
     * Add columns to admin panel for gallery collection post type
     *
     * @param string $post_type_name (Required)
     *                               Post type key name of post type to add this columns
     *
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns
     * @link https://codex.wordpress.org/Plugin_API/Action_Reference/manage_$post_type_posts_custom_column
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$post_type_posts_columns
     */
    protected function addColumnsToAdminPanel($post_type_name){

    }

    //______________________________________________________________________________________________
    // META BOXES
    public function addDefaultMetaBoxes(){
      $this->addMetaBox_selectGalleries($this->gallery_postType_keyName);
    }

    /**
     * Create meta box (meta box type 'select') and add select options contains template files
     *
     * @param array $options (Required)
     *                       Array of html select options arrays. This contains gallery template
     *                       options.
     *                       <code>
     *                       array $options =
     *                       [
     *                       [
     *                       'label' =>  string // (Required) option html tag text. Text label of
     *                       the template file.
     *                       'value' =>  string // (Required) option html tag value. File name of
     *                       the gallery template.
     *                       ],
     *                       [
     *                       ...
     *                       ]
     *                       ]
     *                       </code>
     *
     * @return MetaBoxBase
     * @see  add_meta_box
     * @see  addMetaBox
     * @link https://codex.wordpress.org/Function_Reference/add_meta_box
     * @link http://www.w3schools.com/tags/tag_option.asp
     */
    public function addMetaBox_templateOptions(array $options){
      $meta__galleryCollection__template__callback_args = [
        'meta_boxes' => [
          [
            'type'     => 'select',
            'desc'     => 'Select template for this Gallery Collection post page.',
            'settings' => [
              'select_options' => $options,
            ],
          ],
        ],
      ];

      return parent::addMetaBox('template', 'Gallery Collection Template', $meta__galleryCollection__template__callback_args);
    }


    public function addMetaBox_selectGalleries($gallery_postType_keyName){
      //____________________________________________________________________________________________
      //Get list of all gallery ID by post type
      $wp_q = new WP_Query(['post_type' => $gallery_postType_keyName, 'posts_per_page' => - 1]);
      $selected_gallery_posts = $wp_q->posts;

      //____________________________________________________________________________________________
      // Set post save callback
      $callback_postSave__addCollectionToGallery = function($post_id, $meta_key, $new_val, $old_val){
        $this->addCollectionToGallery($post_id, $meta_key, $new_val, $old_val);
      };

      //____________________________________________________________________________________________
      // Add Gallery Post to the Meta Box
      $galleries_sortable_array = [];
      foreach ($selected_gallery_posts as $gallery_post){
        $gallery_sortable = $this->getGalleryPostMetaBoxSortableEntity($gallery_post);
        array_push($galleries_sortable_array, $gallery_sortable);
      }

      $meta_IdName_base = 'galleryselection_id';
      $meta__gallerysortable__callback_args = [
        'meta_boxes' => [
          [
            'type'     => 'sortable_select',
            'key'      => $this->WEBMATO_PREFIX_META . $meta_IdName_base,
            'desc'     => 'Drag and drop selected galleries to include them in this Gallery Collection.',
            'settings' => [
              'items'             => $galleries_sortable_array,
              'label_selection'   => 'Selected Galleries:',
              'label_options'     => 'Available Galleries:',
              'callback_postSave' => &$callback_postSave__addCollectionToGallery,

            ],
          ],
        ],
      ];

      parent::addMetaBox($meta_IdName_base, 'Selected Galleries', $meta__gallerysortable__callback_args);

    }

    /**
     * @param $current_gallery_collection_id
     * @param $meta_key
     * @param $new_val
     * @param $old_val
     */
    public function addCollectionToGallery($current_gallery_collection_id, $meta_key, $new_val, $old_val){
      $new_selected_galleries_array = json_decode($new_val);
      $old_selected_galleries_array = json_decode($old_val);

      /*
			 * Nothing changed
			 */
      if ($new_val == $old_val){
        //return;
      }

      /*
			 * Delete saved Gallery Collection ID from Gallery meta if is not no more in the Gallery Collection sortable selection
			 */
      if ($old_selected_galleries_array){
        $selection_to_unset = array_diff($old_selected_galleries_array, $new_selected_galleries_array);

        foreach ($selection_to_unset as $key_unset => $unset_selected_gallery_id){
          $old_gallery_meta__gallery_collections_id = get_post_meta($unset_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id');
          $old_gallery_meta__gallery_collections_id = $old_gallery_meta__gallery_collections_id[0];
          $updated_gallery_meta__gallery_collections_id = $old_gallery_meta__gallery_collections_id;
          if ($old_gallery_meta__gallery_collections_id){
            if (($keys_to_delete = array_keys($old_gallery_meta__gallery_collections_id, $current_gallery_collection_id)) !== false){
              for ($i = 0; $i < count($keys_to_delete); $i ++){
                unset($updated_gallery_meta__gallery_collections_id[$keys_to_delete[$i]]);
              }
              unset($i);
            }
          }

          if (count($updated_gallery_meta__gallery_collections_id) > 0){
            update_post_meta($unset_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id', $updated_gallery_meta__gallery_collections_id);
          } else {
            delete_post_meta($unset_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id');
          }

        }
        unset($key_unset);
        unset($unset_selected_gallery_id);
        unset($selection_to_unset);
        unset($old_gallery_meta__gallery_collections_id);
      }


      if ($new_selected_galleries_array){

        foreach ($new_selected_galleries_array as $key => $new_selected_gallery_id){
          $old_gallery_meta__gallery_collections_id = get_post_meta($new_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id');
          $old_gallery_meta__gallery_collections_id = $old_gallery_meta__gallery_collections_id[0];
          $new_gallery_collections_id_array = [];

          /*
					 * Is selected a gallery
					 */
          if ($new_selected_gallery_id){

            // save gallery collection ID to gallery child

            // IF IS there are already set any Gallery Collection to this Gallery OR NOT
            if (count($old_gallery_meta__gallery_collections_id) > 0){
              $new_gallery_collections_id_array = $old_gallery_meta__gallery_collections_id;

              if ( !in_array($current_gallery_collection_id, $old_gallery_meta__gallery_collections_id)){
                array_push($new_gallery_collections_id_array, $current_gallery_collection_id);
                update_post_meta($new_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id', $new_gallery_collections_id_array);
              }


            } else {
              array_push($new_gallery_collections_id_array, $current_gallery_collection_id);
              // Set new Gallery Collections ID to Gallery Meta
              update_post_meta($new_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id', $new_gallery_collections_id_array);
            }
            unset($new_gallery_collections_id_array);
          } else {
            /*
						 * Is selected none gallery
						 */

            // if old meta is same as actual gallery collection ID
            if (is_array($old_gallery_meta__gallery_collections_id)){
              foreach ($old_gallery_meta__gallery_collections_id as $k => $meta){
                if ($current_gallery_collection_id === intval($meta)){
                  // delete old meta
                  unset($old_gallery_meta__gallery_collections_id[$k]);
                }
              }
              update_post_meta($new_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id', $old_gallery_meta__gallery_collections_id);
            } else {
              if ($current_gallery_collection_id === intval($old_gallery_meta__gallery_collections_id)){
                // delete meta
                delete_post_meta($new_selected_gallery_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id');
              } else {
                // leave old meta
              }
            }
          }
        }
        unset($key);
        unset($new_selected_gallery_id);
      }


    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/
    /**
     * @param $gallery_post
     *
     * @return array
     */
    function getGalleryPostMetaBoxSortableEntity($gallery_post){
      global $wpdb;

      $gallery_post_id = $gallery_post->ID;
      $html_id = "sortable_gallery_$gallery_post_id";
      $title = $gallery_post->post_title;
      $gallery_url = get_post_permalink($gallery_post_id);
      $parent_gallery_collections_id_array = get_post_meta($gallery_post_id, $this->WEBMATO_PREFIX_META . 'gallery_collections_id');
      // FIXME:
      $parent_gallery_collections_id_array = $parent_gallery_collections_id_array[0];
      if ($parent_gallery_collections_id_array){
        sort($parent_gallery_collections_id_array);
      }

      //____________________________________________________________________________________________
      // Generate html
      $parent_gallery_coll_html = '';
      if (count($parent_gallery_collections_id_array) == 0){
        $parent_gallery_coll_html = "- - -";
      } else {

        for ($i = 0; $i < count($parent_gallery_collections_id_array); $i ++){
          $gallery_id = $parent_gallery_collections_id_array[$i];
          $gallery_coll_url = get_post_permalink($gallery_id);
          $gallery_coll_title = get_the_title($gallery_id);
          $parent_gallery_coll_html .= "<a href='$gallery_coll_url' target='_blank'>$gallery_id - $gallery_coll_title</a>";
          if (count($parent_gallery_collections_id_array) > $i + 1){
            $parent_gallery_coll_html .= ' | ';
          }
        }
        unset($i);
        unset($gallery_coll_url);
        unset($gallery_coll_title);
        unset($gallery_id);
      }


      $total_images = $wpdb->get_var("SELECT COUNT('ID') FROM {$wpdb->prefix}posts WHERE post_type = 'attachment' && post_parent=$gallery_post_id");
      $tn_w = "50px";
      $tn_h = "50px";
      if (has_post_thumbnail($gallery_post_id)){
        $tn_url = wp_get_attachment_image_src(get_post_thumbnail_id($gallery_post_id), 'thumbnail')['0'];
      } else {
        $tn_url = "http://placehold.it/" . $tn_w . "x" . $tn_h;
      }

      $content_html = "
					<div style='background-color: lightgrey; padding: 3px 5px 2px; border-radius: 5px; border: 1px solid darkgray; cursor: move;'>
						<table>
							
							<tr>
								<td>
									<img src='$tn_url' width='$tn_w' height='$tn_h' style='vertical-align: middle;'>
								</td>
								<td>
									<div style='display: inline-block; vertical-align: middle;'>
										<div style='font-weight: bold; display: block'><a href='$gallery_url' target='_blank'>$title</a></div>
										<div>Gallery post ID: <a href='$gallery_url' target='_blank'>$gallery_post_id</a> with $total_images images.</div>
										<div>Included in Gallery Collection ID - title: $parent_gallery_coll_html</div>
									</div>
								</td>
							</tr>
							
						</table>
					</div>
					";

      //____________________________________________________________________________________________
      // Return
      return $gallery_sortable = [
        'type'         => 'sortable_item',
        'key'          => $html_id,
        'id'           => $html_id,
        'value'        => $gallery_post_id,
        'html_content' => $content_html,
        'sort_string'  => $title,
      ];

    }

    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/

  }
