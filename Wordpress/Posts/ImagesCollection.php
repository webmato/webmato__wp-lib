<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\Posts;


  use Webmato\PhpHelper;

  class ImagesCollection {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    /** @var array */
    private $imagePostsCollection;
    /** @var array */
    private $imageTypeSizes;
    /** @var int */
    private $post_id;
    /** @var array */
    private $getPosts_args;

    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/

    /**
     * ImagesCollection constructor.
     *
     * @param        $post_id
     * @param int    $imagesCount
     * @param string $htmlId_prefix
     */
    public function __construct(
      $post_id,
      $imagesCount = - 1,
      $htmlId_prefix = 'gallery_image_'
    ){
      $this->post_id = $post_id;
      $this->getPosts_args = [
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'posts_per_page' => $imagesCount,
        'post_status'    => 'any',
        'post_parent'    => $post_id,
      ];
      //
      $this->setGlobalImageTypeSizes();
      $this->imagePostsCollection = get_posts($this->getPosts_args);

      foreach ($this->imagePostsCollection as &$imagePost){
        $imagePostId = $imagePost->ID;

        $imagePost->webmato = [];
        $imagePost->webmato['imageSizes'] = [];
        $imagePost->webmato['src'] = [];
        $imagePost->webmato['srcset'] = [];
        $imagePost->webmato['sizes'] = [];

        $imagePost->webmato['meta'] = get_post_meta($imagePostId, '_wp_attachment_metadata')[0];
        $imagePost->webmato['alt'] = isset(get_post_meta($imagePostId, '_wp_attachment_image_alt')[0])
          ? get_post_meta($imagePostId, '_wp_attachment_image_alt')[0] : $imagePost->post_title;

        foreach ($this->imageTypeSizes['other'] as $imageSize){
          $imagePost->webmato['src'][$imageSize] = wp_get_attachment_image_src($imagePostId, $imageSize);
        }
        unset($imageSize);

        if (isset($imagePost->webmato['meta']) && isset($imagePost->webmato['meta']['sizes'])){
          $sizes = $imagePost->webmato['meta']['sizes'];
          $imagePost->webmato['imageSizes'] = $this->setIndividualImageTypeSizes($sizes);
        }

        if ($this->imageTypeSizes['full']){
          $fullSize = reset($this->imageTypeSizes['full']);
          $imagePost->webmato['srcset']['full'] = wp_get_attachment_image_srcset($imagePostId, $fullSize);
          $imagePost->webmato['sizes']['full'] = null;
        }
        if ($this->imageTypeSizes['thumbnail']){
          $thumbnailSize = reset($this->imageTypeSizes['thumbnail']);
          $imagePost->webmato['srcset']['thumbnail'] = wp_get_attachment_image_srcset($imagePostId, $thumbnailSize);
          $imagePost->webmato['sizes']['thumbnail'] = null;
        }
      }
    }

    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/
    private function setGlobalImageTypeSizes(){
      $allImageSizes = get_intermediate_image_sizes();

      $this->imageTypeSizes['full'] = array_values(array_filter($allImageSizes, function($value){
        if (preg_match("/^full_(.*)/i", $value)){
          return true;
        } else {
          return false;
        }
      }));

      $this->imageTypeSizes['thumbnail'] = array_values(array_filter($allImageSizes, function($value){
        if (preg_match("/^thumbnail_(.*)/i", $value)){
          return true;
        } else {
          return false;
        }
      }));

      $this->imageTypeSizes['other'] = array_values(array_filter($allImageSizes, function($value){
        if (preg_match("/(^full_(.*))|(^thumbnail_(.*))/i", $value)){
          return false;
        } else {
          return true;
        }
      }));
    }

    /**
     * @param array $sizes
     *
     * @return mixed
     */
    private function setIndividualImageTypeSizes(array $sizes){
      $imageTypeSizes = [];
      $imageTypeSizes['full'] = [];
      $imageTypeSizes['thumbnail'] = [];
      $imageTypeSizes['other'] = [];

      foreach ($sizes as $sizeName => $sizeValue){
        if (preg_match("/^full_(.*)/i", $sizeName)){
          $sizeValue['name'] = $sizeName;
          array_push($imageTypeSizes['full'], $sizeValue);
        }
      }

      foreach ($sizes as $sizeName => $sizeValue){
        if (preg_match("/^thumbnail_(.*)/i", $sizeName)){
          $sizeValue['name'] = $sizeName;
          array_push($imageTypeSizes['thumbnail'], $sizeValue);
        }
      }

      foreach ($sizes as $sizeName => $sizeValue){
        if ( !preg_match("/(^full_(.*))|(^thumbnail_(.*))/i", $sizeName)){
          $sizeValue['name'] = $sizeName;
          array_push($imageTypeSizes['other'], $sizeValue);
        }
      }

      return $imageTypeSizes;
    }


    /**
     * Find widest image type size object or wide as passed parameter $targetLength or closest
     * wider or more narrow.
     *
     * @param array  $imageSizes
     * @param int    $targetLength
     *
     * @param string $longerOrShorter (Optional) (Default='longer')
     *                                Can be "shorter" or "longer".
     *
     * @return mixed
     */
    static function getWidestImageTypeSize(array $imageSizes, $targetLength = null, $longerOrShorter = 'longer'){

      $result = null;
      $sortedImageSizes = PhpHelper::sortArray($imageSizes, 'width');

      // set initial size
      $result = $sortedImageSizes[0];

      for ($i = 0; $i < count($sortedImageSizes); $i ++){
        $resultLength = $result['width'];
        $currentLength = $sortedImageSizes[$i]['width'];
        $nextLength = isset($sortedImageSizes[$i + 1]) ? $sortedImageSizes[$i + 1]['width'] : null;

        if ($currentLength === $targetLength){
          $result = $sortedImageSizes[$i];
          break;
        } elseif ($currentLength < $targetLength) {
          $result = $sortedImageSizes[$i];
        } elseif ($currentLength > $targetLength && $longerOrShorter == 'longer') {
          $result = $sortedImageSizes[$i];
          break;
        } else {
          break;
        }

      }
      unset($i, $resultLength, $currentLength, $nextLength);

      return $result;
    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/


    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/
    /**
     * @return array
     */
    public function getImagePostsCollection(){
      return $this->imagePostsCollection;
    }

    /**
     * @return int
     */
    public function getPostId(){
      return $this->post_id;
    }

    /**
     * @return array
     */
    public function getImageTypeSizes(){
      return $this->imageTypeSizes;
    }

    /**
     * @return array
     */
    public function getGetPostsArgs(){
      return $this->getPosts_args;
    }
  }
