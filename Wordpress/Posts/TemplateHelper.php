<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato\Wordpress\Posts;


  class TemplateHelper {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    protected $post_id;

    protected $template_meta_array;
    protected $template_file_path;
    protected $template_file; // xxxx-yyy_zzz.php
    protected $template_file_name; // xxxx-yyy_zzz
    protected $template_file_extension; // php
    protected $template_slug; // xxxx
    protected $template_name; // yyy_zzz
    protected $template_html_class;

    /** @var string $WEBMATO_RELATIVE_DIR__TEMPLATES */
    private $WEBMATO_RELATIVE_DIR__TEMPLATES;


    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/

    /**
     * @param int    $post_id           (Required)
     *                                  Post ID
     *
     * @param string $meta_template_key (Required)
     *                                  Post Meta Key of template file
     *
     */
    function __construct($post_id, $meta_template_key){
      if (defined('WEBMATO_RELATIVE_DIR__TEMPLATES')){
        $this->WEBMATO_RELATIVE_DIR__TEMPLATES = WEBMATO_RELATIVE_DIR__TEMPLATES;
      } else {
        $this->WEBMATO_RELATIVE_DIR__TEMPLATES = '';
      }
      //
      $this->post_id = $post_id;

      // Get Template Meta
      $this->template_meta_array = get_post_meta($post_id, $meta_template_key);

      // fixme: check if key of array template_meta_array[0] exists
      // Parse Template File
      $this->template_file = $this->template_meta_array[0];
      $template_info = pathinfo($this->template_file);
      $this->template_file_name = $template_info['filename'];
      $this->template_file_extension = $template_info['extension'];
      $template_filename_arr = explode('-', $this->template_file_name, 2);
      $this->template_slug = $template_filename_arr[0];
      $this->template_name = $template_filename_arr[1];
      $this->template_html_class = $this->parseTemplateHtmlClass($this->template_name);

      // Parse Template File Path
      $this->template_file_path = get_template_directory() . "/$this->template_file";

      // Check if Template File exists
      $this->checkTemplateFile($this->template_file_path);
    }


    /***********************************************************************************************
     *                                       METHODS
     **********************************************************************************************/


    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/

    /**
     * Replace "__" by "-".
     * For example: "webmato__gallery__grid" -> "webmato-gallery webmato-gallery-grid"
     *
     * @param String $templateName
     *
     * @return String
     */
    public function parseTemplateHtmlClass($templateName = ''){
      $templateNameParts = explode('__', $templateName);
      //
      $template_html_classes = [];
      $incrementPartName = null;
      foreach ($templateNameParts as $namePart){
        if ($incrementPartName){
          $incrementPartName = "$incrementPartName-$namePart";
        } else {
          $incrementPartName = $namePart;
        }
        array_push($template_html_classes, $incrementPartName);
      }
      //
      $template_html_class = implode(' ', $template_html_classes);
      $template_html_class = str_replace('_', '-', $template_html_class);

      return $template_html_class;
    }

    /**
     * Check if required template file exists.
     *
     * @param string $template_file_and_path
     *
     * @return bool
     * @throws \ErrorException
     */
    public function checkTemplateFile($template_file_and_path){
      if ( !file_exists($template_file_and_path)){
        throw new \ErrorException('Template file ' . $template_file_and_path . ' not found!');
      }

      return true;
    }

    /***********************************************************************************************
     *                                       GET/SET
     **********************************************************************************************/

    /**
     * @return mixed
     */
    public function getTemplateSlug(){
      return $this->template_slug;
    }

    /**
     * @return mixed
     */
    public function getTemplateName(){
      return $this->template_name;
    }


    /**
     * @return mixed
     */
    public function getTemplateHtmlClass(){
      return $this->template_html_class;
    }

    /**
     * @return mixed
     */
    public function getTemplateFileName(){
      return $this->template_file_name;
    }

    /**
     * @return mixed
     */
    public function getTemplateFileExtension(){
      return $this->template_file_extension;
    }

    /**
     * @return mixed
     */
    public function getTemplateFile(){
      return $this->template_file;
    }

    /**
     * @return mixed
     */
    public function getTemplateFilePath(){
      return $this->template_file_path;
    }
  }
