<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  use Webmato\Wordpress\Helper;
  use \Webmato\Wordpress\Posts\ImagesCollection;

  //________________________________________________________________________________________________
  // DEFINE VARS
  $template_name = $template_info->getTemplateName();
  $template_html_class = $template_info->getTemplateHtmlClass();
  $post_id = $post->ID;
  $post_title = $post->post_title;
  $WEBMATO_PREFIX_META = defined('WEBMATO_PREFIX_META') ? WEBMATO_PREFIX_META : '';
  $galleryCollection_selection_meta_key = $WEBMATO_PREFIX_META . 'galleryselection_id';
  $collection_meta = get_post_meta($post_id, $galleryCollection_selection_meta_key);
  $collection_gallery_ids = [];


  if (is_array($collection_meta)){
    $collection_gallery_ids = json_decode($collection_meta[0]);
  }

  $gallery_collection_header_tabs_html = '';
  $galleries_html = '';

  foreach ($collection_gallery_ids as $key => $current_post_id){
    $gallery_collection_header_tabs_html .= generateHeaderTabsHtml($current_post_id);
    $galleries_html .= generateGalleryHtml($current_post_id);
  }


  //________________________________________________________________________________________________
  // GENERATE HTML
  /**
   * @param Int $post_id
   *
   * @return string
   */
  function generateHeaderTabsHtml($post_id){
    $gallery_title = get_the_title($post_id);

    return "<h2 class='webmato-gallery-collection__tab-item' 
                data-target-post-id='post-$post_id'>$gallery_title</h2>";
  }

  /**
   * @param Int    $post_id
   * @param String $template_html_class
   *
   * @return String
   * @internal param $ {Number} $post_id $post_id
   */
  function generateGalleryHtml($post_id, $template_html_class = ''){
    $collection = new ImagesCollection($post_id);
    $thumbnail_images_html = "";
    $galleryItemClass = 'webmato-gallery-collection-image-item';
    $galleryItemLinkClass = 'webmato-gallery-collection-image-item__link';
    $thumbnailImageClass = 'webmato-gallery-collection-image-item__image';
    $imageCaptionClass = 'webmato-gallery-collection-image-item__image-caption';
    //
    $thumbnailSrcsetObject_keyName = 'full';
    $thumbnailSrcObject_keyName = 'medium';
    $thumbnailSizesObject_keyName = 'full';
    $fullSrcsetObject_keyName = null;


    foreach ($collection->getImagePostsCollection() as $imgPostData){
      $imgPostData->webmato['sizes']['full'] = '100vw';

      $thumbnail_image_html = '';
      require(__DIR__ . '/common/thumbnail_gallery_image.php');
      $thumbnail_images_html .= $thumbnail_image_html;
    }

    $html_classes = implode(' ', get_post_class(['gallery', $template_html_class], $post_id));

    $gallery_html =
      "<article class='webmato-gallery-collection__gallery-article $html_classes'>
        $thumbnail_images_html
      </article>";

    return $gallery_html;
  }


  //________________________________________________________________________________________________
  // ECHO SITE
?>

<?php Helper::visualBlockSeparator_start('GALLERY COLLECTION CONTENT ~ COLUMN FULL VIEW'); ?>

<div <?php post_class(['gallery_collection', $template_html_class]); ?>>
  <nav class="webmato-gallery-collection__tabs"><?= $gallery_collection_header_tabs_html; ?></nav>
  <div class="webmato-gallery-collection__content"><?= $galleries_html; ?></div>
</div>

<?php Helper::visualBlockSeparator_end('GALLERY COLLECTION CONTENT ~ COLUMN FULL VIEW'); ?>

