<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  use Webmato\Wordpress\Helper;
  use \Webmato\Wordpress\Posts\ImagesCollection;

  //________________________________________________________________________________________________
  // DEFINE VARS
  $template_name = $template_info->getTemplateName();
  $template_html_class = $template_info->getTemplateHtmlClass();
  $post_id = $post->ID;
  $post_title = $post->post_title;
  $collection = new ImagesCollection($post_id);
  $thumbnail_images_html_array = [];
  $galleryItemClass = null;

  //________________________________________________________________________________________________
  // GENERATE HTML
  foreach ($collection->getImagePostsCollection() as $imgPostData){
    /*
    $thumbnailWidestSize = ImagesCollection::getWidestImageTypeSize($imgData->webmato['imageSizes']['thumbnail'], 600);
    $fullWidestSize = ImagesCollection::getWidestImageTypeSize($imgData->webmato['imageSizes']['full'], 2560);
    $thumbnailImageSizes = wp_calculate_image_sizes(
      [
        absint($thumbnailWidestSize['width']),
        absint($thumbnailWidestSize['height']),
      ],
      $thumbnailWidestSize['file']
    );
    */
    $thumbnailSrcsetObject_keyName = null;
    $fullSrcsetObject_keyName = null;
    $thumbnailSrcObject_keyName = null;
    $thumbnailSizesObject_keyName = null;
    //
    $imgPostData->webmato['sizes']['thumbnail'] = '33vw';

    $thumbnail_image_html = '';
    require(__DIR__ . '/common/thumbnail_gallery_image.php');
    array_push($thumbnail_images_html_array, $thumbnail_image_html);
  }

  //________________________________________________________________________________________________
  // ECHO SITE
?>

<?php Helper::visualBlockSeparator_start('SINGLE - WEBMATO POST - GALLERY - GALLERY CONTENT ~ GRID'); ?>

<div <?php post_class(['gallery', $template_html_class]); ?> >
<?php
  foreach ($thumbnail_images_html_array as $key => $thumbnail_image_html){
    echo $thumbnail_image_html;
  }
?>
</div>

<?php Helper::visualBlockSeparator_end('SINGLE - WEBMATO POST - GALLERY - GALLERY CONTENT ~ GRID'); ?>

