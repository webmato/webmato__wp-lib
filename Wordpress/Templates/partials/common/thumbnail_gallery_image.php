<?php
  $postId = $imgPostData->ID;
  //
  $thumbnailSrcsetObject_keyName_local = isset($thumbnailSrcsetObject_keyName)
    ? $thumbnailSrcsetObject_keyName : 'thumbnail';
  //
  $fullSrcsetObject_keyName_local = isset($fullSrcsetObject_keyName)
    ? $fullSrcsetObject_keyName : 'full';
  //
  $thumbnailSrcObject_keyName_local = isset($thumbnailSrcObject_keyName)
    ? $thumbnailSrcObject_keyName : 'thumbnail';
  //
  $thumbnailSizesObject_keyName_local = isset($thumbnailSizesObject_keyName)
    ? $thumbnailSizesObject_keyName : 'thumbnail';
  //
  $alt = $imgPostData->webmato['alt'];
  $thumbnailImageSrc = $imgPostData->webmato['src'][$thumbnailSrcObject_keyName_local][0];
  $thumbnailImageSrcSet = $imgPostData->webmato['srcset'][$thumbnailSrcsetObject_keyName_local];
  $thumbnailImageSizes = $imgPostData->webmato['sizes'][$thumbnailSizesObject_keyName_local];
  $fullImageSrcSet = $imgPostData->webmato['srcset'][$fullSrcsetObject_keyName_local];
  $fullImageSrc_json = json_encode($imgPostData->webmato['src']);
  $imageData_json = json_encode($imgPostData);
  $htmlId = "post-$postId-image";
  $targetUrl = $imgPostData->guid;
  $title = $imgPostData->post_title;
  $caption = $imgPostData->post_excerpt;
  //
  $galleryItemClass_local = isset($galleryItemClass)
    ? $galleryItemClass : 'webmato-gallery-image-item';
  //
  $galleryItemLinkClass_local = isset($galleryItemLinkClass)
    ? $galleryItemLinkClass : 'webmato-gallery-image-item__link';
  //
  $thumbnailImageClass_local = isset($thumbnailImageClass)
    ? $thumbnailImageClass : 'webmato-gallery-image-item__thumbnail-image';
  //
  $imageCaptionClass_local = isset($imageCaptionClass)
    ? $imageCaptionClass : 'webmato-gallery-image-item__thumbnail-caption';

  $thumbnail_image_html =
    "<figure class='$galleryItemClass_local'
             data-webmato-post-id='$postId'
             data-webmato-full-image-srcset='$fullImageSrcSet'
             data-webmato-image-src='$fullImageSrc_json'
             data-webmato-post-data='$imageData_json'
     >
      <a class='$galleryItemLinkClass_local'
         href='$targetUrl'
         title='$title'
         target='_blank'>

          <img id='$htmlId'
               class='$thumbnailImageClass_local'
               src='$thumbnailImageSrc'
               srcset='$thumbnailImageSrcSet'
               sizes='$thumbnailImageSizes'
               title='$title'
               alt='$alt'
          >
      </a>
      <figcaption class='$imageCaptionClass_local'>$caption</figcaption>
    </figure>";
