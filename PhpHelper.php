<?php
  /**
   * Copyright (c) 2016. Tomas Hrda <hrda@webmato.net>
   */

  namespace Webmato;


  class PhpHelper {
    /**
     * @param array  $array
     * @param string $keyName
     *
     * @return array
     */
    static function sortArray(array $array, $keyName){
      usort($array, function($a, $b) use ($keyName){
        $val_a = $a[$keyName];
        $val_b = $b[$keyName];

        if ($val_a == $val_b){
          return 0;
        }

        return ($val_a < $val_b) ? - 1 : 1;

      });

      return $array;
    }
  }
